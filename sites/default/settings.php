<?php
  $databases['default']['default'] = array (
    'database' => 'superbook_content',
    'username' => 'sb_content',
    'password' => 'eXGlmJe@!PW4',
    'host' => 'cbn-dmg-mysql-cluster.cluster-cdor3h4gwy77.us-east-1.rds.amazonaws.com',
    'port' => '3306',
    'driver' => 'mysql',
    'prefix' => '',
    'collation' => 'utf8mb4_general_ci',
  );

/**
 * Load services definition file.
 */
$settings['container_yamls'][] = __DIR__ . '/services.yml';

/**
 * Include the Pantheon-specific settings file.
 *

/**
 * If there is a local settings file, then include it
 */
$local_settings = __DIR__ . "/settings.local.php";
if (file_exists($local_settings)) {
  include $local_settings;
}
