<?php
/**
 * Implements hook_skinr_skin_PLUGIN_info().
 */
function superbook_skinr_skin_styles_info() {
  $skins['superbook_font_family'] = array(
    'title' => t('Font-family'),
    'type' => 'select',
    'attached' => array('css' => array('fonts.css')),
    'default status' => 1,
    'group' => 'typography',
    'options' => array(
      'exo' => array('title' => t('Exo'), 'class' => array('red')),
      'color_2' => array('title' => t('Orange'), 'class' => array('orange')),
      'color_3' => array('title' => t('Yellow'), 'class' => array('yellow')),
      'color_4' => array('title' => t('Green'), 'class' => array('green')),
      'color_5' => array('title' => t('Blue'), 'class' => array('blue')),
      'color_6' => array('title' => t('Violet'), 'class' => array('violet')),
    ),
  );
  return $skins;
}
