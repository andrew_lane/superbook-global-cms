'use strict';

var config      = require('./config.json');
var gulp        = require('gulp');
var run         = require('gulp-run');
var runSequence = require('run-sequence');
var sass        = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');

function isDirectory(dir) {
    try {
        return fs.statSync(dir).isDirectory();
    }
    catch (err) {
        return false;
    }
}

gulp.task('watch', ['sass-change'], function() {
    gulp.watch(config.sass.srcFiles, ['sass-change']);
    gulp.watch(config.css, ['autoprefixer']);
});

gulp.task('sass', function() {
    return gulp.src(config.sass.srcFiles)
        .pipe(sass(config.sass.options).on('error', sass.logError))
        .pipe(gulp.dest(config.sass.destDir))
    ;
});

gulp.task('autoprefixer', function () {
  return gulp.src(config.css).pipe(autoprefixer(config.autoprefixer))
});

gulp.task('sass-change', function() {
    runSequence('sass');
});


gulp.task('default', ['watch']);
