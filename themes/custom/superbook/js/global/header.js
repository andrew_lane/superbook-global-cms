
(function header ($) {

  var timer;
  var ctimer;
  var link;
  var moreMenu;
  var friendLink;
  var friendPanel;
  var friendRequest;
  var userLink;
  var userPanel;
  var alertLink;
  var alertPanel;
  var width = screen.width;
  var h = 50;
  var w = 35;

  function init () {
       getMainNavigationLinks()
       mobileMenu();
       moreMenu();
       try {
         alertPanel();
         friendPanel();
         userPanel();
       } catch (e) {}
  }

  function  userPanel () {
    userLink = document.querySelector('.user-link');
    userPanel = document.querySelector('.user-panel');
    userLink.addEventListener('click', toggleUserPanel);
  }

  function toggleUserPanel (e) {
    e.preventDefault();
    try {
      var closeBtn = document.querySelector('.user-cl');
      closeBtn.addEventListener("click", toggleUserPanel);
      closeAlertPanel();
      closeFriendPanel();
      if (userPanel.className == 'user-panel hidden') {
        userPanel.className = 'user-panel';
      } else {
        userPanel.className = 'user-panel hidden';
      }
    } catch (e) {}
  }

  function friendPanel () {
    friendLink = document.getElementById('sb_friends');
    friendsPanel = document.querySelector('.friends-panel');
    friendLink.addEventListener('click', toggleFriendPanel);
  }

  function toggleFriendPanel () {
    try {
      var closeBtn = document.querySelector('.friend-cl');
      var friendRequest = document.querySelectorAll('.friend-request');
      closeAlertPanel();

      if (friendsPanel.className == 'friends-panel hidden') {
        friendsPanel.className = 'friends-panel';
        friendLink.addEventListener("click", toggleFriendPanel);
        closeBtn.addEventListener("click", toggleFriendPanel);
        updateFriendRequestCount();
        // yes no btn
        friendRequest.forEach(function (element) {
          var fid = element.id;
          var yesbtn = element.querySelector('.friend-accept');
          var nobtn = element.querySelector('.friend-decline');
          yesbtn.addEventListener("click", function () {
            acceptFriend(fid);
          });
          nobtn.addEventListener("click", function () {
            declineFriend(fid);
          });
        });

      } else {
        friendsPanel.className = 'friends-panel hidden';
      }
    } catch (e) {

    }
  }

  function updateFriendRequestCount () {
    try { document.querySelector('.friend_badge').remove();} catch (e) {}
    return friendRequest('count', null, null);
  }

  function acceptFriend(fid) {
    try { document.getElementById(fid).className = "friend-request friends"; } catch (e) {}
    return friendRequest('request', 'true', fid);
  }

  function declineFriend(fid) {
    try { document.getElementById(fid).remove()} catch (e) {}
    return friendRequest('request', 'false', fid);
  }

  function updateAlerts () {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        return;
      }
    };
    xhttp.open("GET", "/filter_alerts/ajax/sort", true);
    xhttp.send();
  }

  function friendRequest (filter, bool, fid) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
       // show friends btn and hide yes and no btns.
      }
    };
    xhttp.open("GET", "/friend_request/ajax/"+filter+"/"+bool+"/"+fid, true);
    xhttp.send();
  }

  function closeFriendPanel () {
    try {
      friendsPanel.className = 'friends-panel hidden';
    } catch (e) {}
  }

  function closeAlertPanel () {
    try {
      alertPanel.className = 'alerts-panel hidden';
    } catch (e) {}

  }

  function alertPanel () {
    try {
      alertLink = document.querySelector('.alert-nav');
      alertPanel = document.querySelector('.alerts-panel');
      alertLink.addEventListener("click", toggleAlertPanel);
    }catch(err) {}
  }

  function toggleAlertPanel (e) {
    try {
      var closeBtn = document.querySelector('.alert-cl');
      closeFriendPanel();

      if (alertPanel.className == 'alerts-panel hidden') {
        alertPanel.className = 'alerts-panel';
        updateAlerts();
        document.querySelector('.alert_badge').remove();
        alertLink.addEventListener("click", toggleAlertPanel);
      } else {
        alertPanel.className = 'alerts-panel hidden';
      }
    } catch (e) {

    }
  }

  function getMainNavigationLinks () {
    var links = document.querySelectorAll('[data-drupal-link-system-path]');
    return appendSvgToImagelink(links);
  }

  function mobileMenu (e) {
    var menu = document.querySelector('.menu-toggle');
    menu.addEventListener("click", toggleMobileMenu);
  }

  function toggleMobileMenu (e) {
    e.preventDefault();
    var menu = document.querySelector('.mobile-menu');

    if (menu.className == "mobile-menu hidden") {
      menu.className = 'mobile-menu';
    } else {
      menu.className = "mobile-menu hidden";
    }

    var link = document.querySelector('.menu-toggle');
    if (link.className == "menu-toggle") {
      link.className = "menu-toggle close-menu";
    } else {
      link.className = "menu-toggle";
    }
  }

  function moreMenu () {
    link = document.querySelectorAll('[data-drupal-link-system-path = more]');
    moreMenu = document.querySelector('.more-menu');

    link[0].addEventListener("mouseover", openMoreMenu);
    link[0].addEventListener("mouseleave", function () {
      timer = setTimeout(closeDesktopMoreMenu, 500);
    });
    moreMenu.addEventListener("mouseover", function () {
      clearTimeout(timer);
    });
    moreMenu.addEventListener("mouseleave", function () {
      timer = setTimeout(closeDesktopMoreMenu, 500);
    });
  }

  function closeDesktopMoreMenu () {
    clearTimeout(timer);
    moreMenu.className = 'more-menu hidden';
    link[0].className = 'nav-link';
	}

  function openMoreMenu () {
    clearTimeout(timer);
    moreMenu.className = 'more-menu';
    link[0].className = 'nav-link active';
    closeFriendPanel();
    closeAlertPanel();
  }

	function closeMoreMenu () {
    var more_menu = document.querySelector('.mobile-menu');
    var className = more_menu.className;
    if (className !== 'mobile-menu hidden') {
      more_menu.className = 'mobile-menu hidden';
    }
	}

  function appendSvgToImagelink (links) {
    var gameslink = document.querySelectorAll('[data-drupal-link-system-path = "node/926096"]');
    var gamesContent = gameslink[0].innerHTML;

    var videoslink = document.querySelectorAll('[data-drupal-link-system-path = "node/926101"]');
    var videosContent = videoslink[0].innerHTML;

    var biblelink = document.querySelectorAll('[data-drupal-link-system-path = bible]');
    var bibleContent = biblelink[0].innerHTML;

    var morelink = document.querySelectorAll('[data-drupal-link-system-path = more]');
    var moreContent = morelink[0].innerHTML;

    var contestslink = document.querySelectorAll('[data-drupal-link-system-path = contests]');
    var contestsContent = contestslink[0].innerHTML;

    var radiolink = document.querySelectorAll('[data-drupal-link-system-path = radio]');
    var radioContent = radiolink[0].innerHTML;

    var discoverLink = document.querySelectorAll('[data-drupal-link-system-path = discover]');
    var discoverContent = discoverLink[0].innerHTML;

    var appLink = document.querySelectorAll('[data-drupal-link-system-path = app]');
    var appContent = appLink[0].innerHTML;

    var gamesContent =
    links.forEach(function (element) {
      var dataAttr = element.getAttribute("data-drupal-link-system-path");
      switch (dataAttr) {
        case 'node/926096':
          var link = document.querySelectorAll('[data-drupal-link-system-path ="'+dataAttr+'"]');
          link.forEach(function (element) {
            element.innerHTML ='<svg class="menu-item-viewBox" xmlns="http://www.w3.org/2000/svg" width="'+w+'" height="'+h+'" viewBox="-5666.624 -3795.85 56.122 20"><defs><style>.cls-1 { fill: #fff; }</style></defs><g id="icon-games" class="menu-svg"><path data-name="Path 204" class="cls-1" d="M-5646.242-3793.266l.987 3.6a2.207 2.207 0 0 0 2.73 1.278 2.207 2.207 0 0 0 1.565-2.678l-.988-3.134a2.207 2.207 0 0 0-2.73-1.566 2.207 2.207 0 0 0-1.564 2.67z"/><path id="Path_205" data-name="Path 205" class="cls-1" d="M-5611.28-3780.26a13.652 13.652 0 0 0-.348-1.916 13.534 13.534 0 0 0-7.377-8.713 13.766 13.766 0 0 0-10.978 0 13.592 13.592 0 0 0-5.228 3.777l-8.364 2.21-4.82 1.277a13.766 13.766 0 0 0-6.332-.7l-1.86.35a13.476 13.476 0 0 0-9.64 16.263 13.244 13.244 0 0 0 .638 1.86c.813 2.206 1.626 4.24 2.5 6.156a32.586 32.586 0 0 0 8.362 12.256l.7.464.58.35h2.673a4.53 4.53 0 0 0 1.22 0 14.406 14.406 0 0 0 3.02-1.1l.64-.466.464-.35 1.274-1.28.465-.64a10.514 10.514 0 0 0 .812-1.394 11.618 11.618 0 0 0 .7-1.684 14.23 14.23 0 0 0 .407-1.51v-1.57a43.794 43.794 0 0 0 0-6.505c0-1.86 5.46-3.776 8.654-3.6h1.452l.35.35.754 1.335a31.657 31.657 0 0 0 3.834 5.692 11.21 11.21 0 0 0 7.667 4.182h4.7l.524-.35.64-.522.348-.35.407-.58a11.617 11.617 0 0 0 1.335-3.427v-2.38a55.994 55.994 0 0 0-.174-17.485zm-37.93 9.295l-2.555.7.7 2.556a2.382 2.382 0 0 1-1.69 2.906 2.44 2.44 0 0 1-1.976-.35 2.38 2.38 0 0 1-1.047-1.277l-.7-2.557-2.556.7a2.382 2.382 0 1 1-1.278-4.59l2.555-.7-.7-2.555a2.38 2.38 0 0 1 1.7-2.907 2.44 2.44 0 0 1 1.916.29 2.38 2.38 0 0 1 .988 1.395l.7 2.556 2.556-.7a2.37 2.37 0 0 1 1.394 4.532zm5.81 20.446zm14.173-24.4h-.813a3.08 3.08 0 0 1-2.962-2.264 3.08 3.08 0 0 1 2.146-3.714h.813a3.08 3.08 0 0 1 .813 5.81zm0-8.944a3.08 3.08 0 0 1 2.5-3.776h.813a3.044 3.044 0 0 1 .813 5.983h-.816a3.08 3.08 0 0 1-3.195-2.207zm6.912 12.895a3.08 3.08 0 0 1-3.776-2.09 2.962 2.962 0 0 1 0-1.684 3.06 3.06 0 1 1 3.775 3.776zm4.124-7.085h-.814a3.044 3.044 0 0 1-.813-5.983h.813a3.08 3.08 0 0 1 2.962 2.265 3.02 3.02 0 0 1-2.15 3.892z"/></g></svg><div class="menu-name">'+ gamesContent  + "</div>";
          });
        break;
        case 'node/926101':
          var link = document.querySelectorAll('[data-drupal-link-system-path ="'+dataAttr+'"]');
          link.forEach(function (element) {
            element.innerHTML ='<svg class="menu-item-viewBox" xmlns="http://www.w3.org/2000/svg" width="'+w+'" height="'+h+'" viewBox="-5512.482 -3803.187 61.237 20"><defs><style>.cls-1 { fill: #fff; fill-rule: evenodd; } .cls-2 { fill: none; }</style></defs><g id="icon-video" class="menu-svg" data-name="icon-video"><path id="Path_224" data-name="Path 224" class="cls-1" d="M-5469.58-3794.2c-7.85-4.495-29.18 1.225-33.73 9.045s1.397 29.826 9.196 34.293 29.35-1.398 33.77-9.054-1.476-29.805-9.235-34.283zm-.576 19.57l-13.596 13.655c-1.258.94-2.41 1.466-3.078-.858l-4.91-18.313c-.388-1.45.24-2.006 1.667-1.827l19.436 5.23c1 .508.94 1.256.48 2.114z"/><path id="Rectangle_204" data-name="Rectangle 204" class="cls-2" d="M-5512.482-3790.246l48.3-12.95 12.95 48.3-48.3 12.95z"/></g></svg><div class="menu-name">'+videosContent+ "</div>";
          });
        break;
        case 'bible':
          var link = document.querySelectorAll('[data-drupal-link-system-path ='+dataAttr+']');
          link.forEach(function (element) {
            element.innerHTML ='<svg class="menu-item-viewBox" xmlns="http://www.w3.org/2000/svg" width="'+w+'" height="'+h+'" viewBox="-5354.532 -3792.941 61.205 20"><defs><style>.cls-1 { fill: #fff; }</style></defs><path id="icon-book" class="cls-1 menu-svg" d="M-5352.932-3792.41c10.112 7.984 23.95-2.128 30.336 6.92 2.66-8.515 14.9-5.322 18.628-7.45l10.636 32.464-1.6 2.66.532 1.065c0 .53 0 .53-.532 1.063-.532 0-18.1 6.387-18.1 6.387s-1.052 2.13-4.777 2.66-4.257-2.13-4.257-2.13l-27.675-.53a1.858 1.858 0 0 1-1.064-.53s-3.726-36.192-3.726-36.724 0-1.064 1.064-1.064l2.13 6.92z"/></svg><div class="menu-name">'+bibleContent+ "</div>";
          });
        break;
        case 'more':
          var link = document.querySelectorAll('[data-drupal-link-system-path ='+dataAttr+']');
          link.forEach(function (element) {
            element.innerHTML ='<svg class="menu-item-viewBox" width="'+w+'" height="'+h+'" viewBox="0 0 35 20" xmlns="http://www.w3.org/2000/svg"><title>Icon / More</title><path d="M17.5 28.324l-.14.14L.232 11.337l4.008-4.01 13.26 13.26 13.26-13.26 4.008 4.01-17.13 17.125" fill-rule="nonzero" fill="#FFF"/></svg><div class="menu-name">'+ moreContent + "</div>";
          });

        break;
        case 'radio':
          var link = document.querySelectorAll('[data-drupal-link-system-path ='+dataAttr+']');
          link.forEach(function (element) {
            element.innerHTML ='<svg width="'+w+'" height="'+h+'" class="more-menu-item" viewBox="0 0 35 20" xmlns="http://www.w3.org/2000/svg"><title>Icon / Music</title><path d="M23.663 29.285c-1.96-1.623-2.767-4.258-2.05-6.7.717-2.442 2.82-4.222 5.347-4.527 1.125-.1 2.255.114 3.266.616L29.124 8.232l-9.522.99v.015l-6.33.668 1.67 15.82c.327 3.447-2.188 6.513-5.633 6.864-3.445.35-6.528-2.144-6.903-5.586s2.1-6.54 5.538-6.94c1.125-.098 2.255.115 3.266.616L9.44 3.91l7.818-.825-.002-.015 14.364-1.5 2.337 22.15c.294 2.557-.998 5.037-3.263 6.262-2.264 1.226-5.047.95-7.028-.696h-.003z" fill-rule="nonzero" fill="#FFF"/></svg><div class="menu-name">'+ radioContent +'</div>';
          });
        break;
        case 'contests':
          var link = document.querySelectorAll('[data-drupal-link-system-path ='+dataAttr+']');
            link.forEach(function (element) {
              element.innerHTML ='<svg width="'+w+'" height="'+h+'" class="more-menu-item" viewBox="0 0 35 35" xmlns="http://www.w3.org/2000/svg"><title>Icon / Contest</title><g fill="none" fill-rule="evenodd"><path d="M24.44 2.083l.534 1.993c1.483.148 2.727 1.303 3.058 2.84.4 1.493-.084 3.066-1.227 3.988l.61 2.276c2.258-1.215 3.178-4.207 2.49-6.768-.683-2.576-2.948-4.37-5.465-4.33z" fill="#FFF"/><path d="M25.54 11.548l-.802.215-.376-7.83c.267-.07.267-.07.612.14l-.535-1.99-.268.07c-.497-.78-1.262-1.49-2.065-1.275L4.967 5.474c-.67.087-1.215.594-1.377 1.284l.763 2.846.268-.072s3.63 6.042 3.783 6.61c-.267.072-.727.5-.995.572-.45.17-.943.195-1.416.075l.534 1.99c.474.006.946-.02 1.415-.073.563-.102 1.088-.348 1.532-.715 1.146 2.132 4.55 3.05 6.846 3.044l1.678 6.26c-1.607.43-2.678.717-2.41 2.78l-2.413.648c-1.53.715-.844 3.276.763 2.846l14.46-3.878c.5-.108.895-.498 1.028-1.017.133-.518-.018-1.08-.392-1.46-.374-.384-.91-.525-1.398-.368l-2.41.646c-1.07-1.85-2.14-1.562-3.48-1.203l-1.754-6.544c2.066-.86 4.822-3.43 4.67-6.133.536-.143.88.07 1.416-.074.474-.08.912-.305 1.262-.645l-.534-1.992c-.35.34-.79.565-1.265.645z" fill="#FFF"/><path d="M2.86 13.664c-.46-1.707.307-3.133 1.494-4.06L3.59 6.758c.077.284-.19.356-.114.64C1.562 8.83.144 11.04.983 14.17c.687 2.56 3.173 4.333 5.81 4.54l-.532-1.988c-1.562-.297-2.87-1.47-3.4-3.056z" fill="#FFF"/><path d="M19.88.64c2.604-.483 5.316.332 6.79 2.883 1.512 2.83.7 6.27-1.856 7.876-.48.276-.867.16-1.348.437.846 2.573-1.15 5.768-2.924 7.132l3.388 5.866c1.2-.693 2.16-1.248 3.673.26l2.16-1.247c.43-.278.986-.28 1.446-.01.46.274.75.777.756 1.312.006.535-.274 1.014-.73 1.248l-12.964 7.488c-1.44.832-2.767-1.464-1.473-2.55l2.16-1.25c-.79-1.925.17-2.48 1.61-3.31l-3.24-5.613c-2.217.6-5.742.594-7.4-1.17-.335.47-.78.844-1.295 1.088-2.67 1.412-6.053.395-7.75-2.328-1.62-2.806-.822-5.308.656-7.182-.387-.998-.126-2.108.65-2.757L17.556-.06c.72-.415 1.643.073 2.325.7z"/></g></svg><div class="menu-name">'+ contestsContent +'</div>';
            });
        break;
        case 'discover':
          var link = document.querySelectorAll('[data-drupal-link-system-path ='+dataAttr+']');
          link.forEach(function (element) {
            element.innerHTML ='<svg class="more-menu-item" width="'+w+'" height="'+h+'" viewBox="0 0 35 35" xmlns="http://www.w3.org/2000/svg"><title>Icon / Gospel</title><path d="M14.8 34.59V14.41H5V9.224h9.8V0h5.764v9.224h9.8v5.188h-9.8V34.59" fill-rule="nonzero" fill="#FFF"/></svg><div class="menu-name">'+ discoverContent +'</div>';
          });
        break;
        case 'app':
          var link = document.querySelectorAll('[data-drupal-link-system-path ='+dataAttr+']');
          link.forEach(function (element) {
            element.innerHTML ='<svg class="more-menu-item" width="'+w+'" height="'+h+'" viewBox="0 0 35 35" xmlns="http://www.w3.org/2000/svg"><title>Icon / App</title><g fill-rule="nonzero" fill="#FFF"><path d="M25.932 34.505H8.496c-2.366-.003-4.283-1.92-4.286-4.286V4.78C4.213 2.414 6.13.497 8.496.494h17.436c2.366.003 4.283 1.92 4.286 4.286v25.44c-.004 2.366-1.92 4.282-4.286 4.285zM8.212 5.83v23.34h18.005V5.83H8.21z"/><path d="M16.136 24.008V16.67H12.57v-1.884h3.566V11.43h2.096v3.356h3.564v1.887h-3.564v7.338"/></g></svg><div class="menu-name">'+ appContent +'</div>';
          });
        break;
      }
    });
  }
  init();
})($)
