(function footer ($) {
  function init() {
    appendMaterialIcon();
  }
  function appendMaterialIcon () {
    var links = document.querySelectorAll('.menu--footer .nav-link');
      links.forEach(function (element) {
        var dataAttr = element.getAttribute("data-drupal-link-system-path");
        switch (dataAttr) {
          case 'node/926092':
            content = element.innerHTML;
            element.innerHTML = content  + '<i class="material-icons">&#xE315;</i>';
          break;
          case 'node/926093':
            content = element.innerHTML;
            element.innerHTML = content  + '<i class="material-icons">&#xE315;</i>';
          break;
          case 'node/926094':
            content = element.innerHTML;
            element.innerHTML = content  + '<i class="material-icons">&#xE315;</i>';
          break;
          case 'node/926095':
            content = element.innerHTML;
            element.innerHTML = content  + '<i class="material-icons">&#xE315;</i>';
          break;
          case 'contact':
          content = element.innerHTML;
          element.innerHTML = content  + '<i class="material-icons">&#xE316;</i>';
          break;
        }
      });
    }
    return init();
 })($)
