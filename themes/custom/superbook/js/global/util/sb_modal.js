/**
 * @file
 */

// global
var sb_modal = {};

(function ($) {

	var modal = null;
	var cid = null; // id of element
	var closeBtn = null;

	try { modal = document.getElementsByClassName('generic-modal')[0]; } catch(err) {}
	try { closeBtn = document.getElementsByClassName('close-generic-modal')[0]; } catch(err) {}

	sb_modal.toggleModal = function (_id) {
		if (modal.className !== 'modal-overlay generic-modal modal-show') {
			cid = _id;
			sb_modal.openModal();
		} else {
			sb_modal.closeModal();
		}
	};
	sb_modal.openModal = function () {
		modal.className = modal.className + " modal-show";
		document.addEventListener("keydown", sb_modal.keyListener);
		closeBtn.addEventListener("click", sb_modal.closeModal);
	};

	sb_modal.keyListener = function (event) {
		if (event.which === 27) {
			sb_modal.closeModal();
		}
	};

	sb_modal.closeModal = function () {
		modal.className = "modal-overlay generic-modal";
		document.removeEventListener("keydown", sb_modal.keyListener);
		closeBtn.removeEventListener("click", sb_modal.closeModal);
	};

	sb_modal.setContent = function (_content) {
		var content_area = modal.getElementsByClassName('generic-modal-content-area')[0];
		try{ content_area.innerHTML = _content; }catch(err){}
	};

	sb_modal.setTitle = function (_title) {
		var title_area = modal.getElementsByClassName('generic-modal-title-area')[0];
		try{ title_area.innerHTML = _title; }catch(err){}
	};
})(jQuery);
