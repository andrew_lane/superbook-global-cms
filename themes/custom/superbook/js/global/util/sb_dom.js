/**
 * @file
 */
var sb_dom = {};

(function () {
    'use strict';
    
    return sb_dom = {
    doc: document.documentElement,
    eventUtil: {
        /**
            @public
            @description addHandler - backwards compatible addEventListner
            @param {element, type, handler}  element, type = load events or mouse events(click, dbclick, mousedown, mouseenter, mouseleave, mousemove, mouseout, mouseover mouseup), handler = call back function
        */
        addHandler: function (element, type, handler) {

            if (element != null) {
                if (element.addEventListner) { // W3C DOM
                    element.addEventListner(type, handler, false);
                } else if (element.attachEvent) { // IE DOM
                    element.attachEvent('on' + type, handler);
                } else {
                    element['on' + type] = handler;
                }
            }
        },

        removeHandler: function (element, type, handler) {

            if (element != null) {
                if (element.removeEventListner) { // W3C DOM
                    element.removeEventListner(type, handler, false);
                } else if (element.removeEvent) { // IE DOM
                    element.removeEvent('on' + type, handler);
                } else {
                    element['on' + type] = handler;
                }
            }
        },
        /**
            @public
            @description getEvent - returns the backwards complatible current event.
            @param {event} event - mouse or load event
        */
        getEvent: function (event) {
            return event ? event : window.event;
        },
        /**
            @public
            @description getTarget - returns the backwards complatible current target
            @param {event} event - mouse or load event
        */
        getTarget: function (event) {
            return event.target || event.srcElement;
        },

        getCurrentTarget: function (event) {

            var e = event ? event : window.event;
            var obj = null;
            
            if (e.currentTarget) {
                obj = e.currentTarget;
            } else {
                obj = e.srcElement;
                obj = obj.parentNode;
            }

            return obj;
        },

        /**
            @public
            @description getRelatedTarget - returns the backwards complatible parent target
            @param {event} event - mouse or load event
        */
        getRelatedTarget: function(event) {

            var e = event ? event : window.event;

            if (e.relatedTarget) {
                return e.relatedTarget;
            } else if (e.toElement) {
                return e.toElement;
            } else if (e.fromElement) {
                return e.fromElement;
            } else {
                return null;
            }
        },
        /**
            @public
            @description preventDefault - stops defualt event
            @param {event} event - mouse or load event
        */
        preventDefault: function (event) {

            var e = event ? event : window.event;

            if (e.preventDefault) {

                e.preventDefault();

            } else {

                e.returnValue = false;
            }
        },
        /**
            @public
            @description removeHandler - removes current attached event listener
            @param {element, type, handler} element, type, handler
        */
        removeHandler: function (element, type, handler) {
            if (element.removeEventListner) {
                element.removeEventListner(type, handler, false);
            } else if (element.detachEvent) {
                element.detachEvent('on' + type, handler);
            } else {
                element['on' + type] = null;
            }
        },
        /**
            @public
            @description stopPropagation - stops event bubbling.
            @param {event} event
        */
        stopPropagation: function (event) {
            var e = event ? event : window.event;

            if (e.stopPropagation) {
                e.stopPropagation();
            } else {
                e.cancelBubble = true;
            }
        }
    },
    /**
        @public
        @description removeElementFromBody deletes a element from the body
         @param {id} id = id of element
    */
    removeElementFromBody: function (id) {
        var d = document.getElementsByTagName('body')[0];
        var element = document.getElementById(id);

        d.removeChild(element); 
    },
    
    getLanguage: function () {
        var language = document.documentElement.lang;
        return language;
    },
    
    getDirection: function () {
        var direction = document.documentElement.dir;
        return direction;
    },
    /**
        @public
        @description trim removes spaces from the passed parameter.
        @param {str} String.
    */
    trim: function (str){
        if (str) {
            return str.trim ? str.trim() : str.replace(/^\s+|\s+$/g,'');
        }
    },

    strip: function (html)
    {
       var tmp = document.createElement("DIV");
       tmp.innerHTML = html;
       return tmp.textContent || tmp.innerText || "";
    },
    /**
        @public
        @description log safely logs to the browser console.
        @param {whatever} Object, Array, String.
    */
    log: function (whatever) {
        try {
            console.log(whatever);
        } catch (e) {}
    },
    /**
        @public
        @description hasClass checks to see if the class name already exists on the parameter.
        @param {el, cn} el = Object (DOM element), cn = String (classname without a dot)
    */
    hasClass: function ( el, cn) {
        if (el) {
            return (' ' + el.className + ' ').indexOf(' ' + cn + ' ') !== -1;
        }
    },
    /**
        @public
        @description id returns the tag obj
        @param {id} id = id of element
    */

    id: function(id){
        return document.getElementById(id);
    },
    /**
         * class searches the document for the parameter classname. Use this instead of querySelectorAll(), as this is IE5+ compatable.
         * @param  {[string]} cn [classname without a dot]
         * @return {[array]}    [returns an array of the dom elements found]
     */
    class: function (cn) {
      var matches = [],
        elements = document.getElementsByTagName('*'),
        length = elements.length;

      for (var i = 0; i < length; i++) {
        if (this.hasClass(elements[i], cn) === true) {
          // Element exists with attribute. Add to array.
          matches.push(elements[i]);
        }
      }

      // clear value
      elements = null;
      return matches;
    },

    /**
        @public
        @description inputfieldStyler - styles the input field with a error style or success
        @param {conditon, id} condition = string (error, success), obj = DOM Element
    */
    inputfieldStyler: function (condition, obj) {

        var sibling;
        var test;

        try {
            test = obj.parentNode.childNodes[3].className;
            sibling = obj.parentNode.childNodes[3];
        } catch(err) {
            sibling = obj.parentNode.childNodes[2];
        }
        

        switch(condition) {
            case "error":
                obj.parentNode.className = "control-group error"; 
                sibling.className = 'input-icon fui-cross';  
            break;
            case "success":
                obj.parentNode.className = "control-group success";
                sibling.className = 'input-icon fui-check-inverted'; 
            break;
        }
    },

    /**
        @public
        @description validateEmail - validates email addresses
        @param { email} email = jeff@test.com
    */
    validateEmail: function (email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    },
    
    /**
        @public
        @description addClassToChildEl adds a class to a nested element.
        @param {el_cn, parent_id, cn} el_cn = element's class name, parent_id = parent elements id, cn = Class name to add to element
    */
    addClassToChildEl: function (el_cn, parent_id, cn) {
        var children = this.class(el_cn),
            length = children.length;

        for (var i = 0; i < length; i++) {
            if (this.hasParent(children[i], parent_id) === true) {
                this.addClass(children[i], cn);
            }
        }
    },
    /**
        @public
        @description removeClassToChildEl removes a class to a nested element.
        @param {el_cn, parent_id, cn} el_cn = element's class name, parent_id = parent elements id, cn = Class name to remove from element
    */
    removeClassToChildEl: function (el_cn, parent_id, cn) {
        var children = this.class(el_cn),
            length = children.length;

        for (var i = 0; i < length; i++) {
            if (this.hasParent(children[i], parent_id) === true) {
                this.removeClass(children[i], cn);
            }
        }
    },
    /**
        @public
        @description addClass adds a class to the element in the parameter.
        @param {el, cn} el = Object (DOM element), cn = String (classname without a dot)
    */
    addClass: function (el, cn) {
        if (el && !this.hasClass(el, cn)) {
            el.className = (el.className === '') ? cn : el.className + ' ' + cn;
        }
    },
    /**
        @public
        @description removeClass removes the class name from the cparameter.
         @param {el, cn} el = Object (DOM element), cn = String (classname without a dot)
    */
    removeClass: function (el, cn) {
        if (!this.hasClass(el,'disabled')) {
            try {
                el.className = this.trim((' ' + el.className + ' ').replace(' ' + cn + ' ', ' '));
            }catch(err){
               return; 
            }
            
        }
    },
    /**
        @public
        @description removeAllClasses removes all classnames from the document
         @param {elcn, cn} elcn = String (classname), cn = String (classname)
    */
    removeAllClasses: function (elcn, cn) {
        var groups = this.class(elcn),
        groupslth = groups.length;

        for (var i = 0; i < groupslth; i++) {
            this.removeClass(groups[i], cn);
        }
        // dump the values
        groups = null;
        groupslth = 0;
    },
    /**
        @public
        @description profileImgError replaces the missing image with a noimage file and returns true.
        @param {image, replace} image:Object, replace:String.
    */
    profileImgError: function (image, replace) {
        image.onerror = '';
        switch (replace) {
            case 'avatar':
                image.src = '/sites/all/themes/custom/superbook/images/content/noimage_avatar.jpg';
            break;
            case 'avatar_sm':
                image.src = '/sites/all/themes/custom/superbook/images/content/noimage_avatar_sm.jpg';
            break;
        }
        return true;
    },

    isIE6: function () {
        var isIE = false,
            ie = document.getElementsByTagName('body')[0],
            cName = ie.parentNode.className.substr(2,4);
            cName = this.trim(cName);

        if ((cName == "ie8") || (cName == "ie6")) {
            isIE = true;
        }


        return isIE;
    },

    browser: function (display) {
        var ua = navigator.userAgent, tem, 
            M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];

        if (/trident/i.test(M[1])) {
            
            tem =  /\brv[ :]+(\d+)/g.exec(ua) || [];
            
            return 'IE '+(tem[1] || '');
        }

        if (M[1] === 'Chrome') {
            
            tem = ua.match(/\bOPR\/(\d+)/)
            
            if (tem != null) {
                return 'Opera ' + tem[1];
            } 
        }

        M = M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];

        if ((tem = ua.match(/version\/(\d+)/i))!= null) {
            M.splice(1, 1, tem[1]);
        }

        switch (display) {
            case 'name':
                var name = M[0];
                return name;
            break;
            case 'version':
                var version = M[1];
                return version;
            break;
            case 'full':
                var full = M.join(' ');
                return full;
            break;
        }
        
    },

    /**
        @public
        @description isMobile checks to see if the browser is a mobile device and returns bool.
        @param {}
    */
    isMobile: function () {
        var pass = false;

        if( navigator.userAgent.match(/Android/i)
         || navigator.userAgent.match(/webOS/i)
         || navigator.userAgent.match(/iPhone/i)
         || navigator.userAgent.match(/iPad/i)
         || navigator.userAgent.match(/iPod/i)
         || navigator.userAgent.match(/BlackBerry/i)
         || navigator.userAgent.match(/Windows Phone/i))
        {
            pass = true;
        }
        
        return pass;
    },
    /**
        @public
        @description is Child check to see if the user is under the age of 13
        @param {year, month, day}
    */
    isChild :function (year, month, day) {

            var born = new Date(parseInt(year), parseInt(month), parseInt(day)),
                now = new Date(),
                birth = Math.floor((now - born) / (1000 * 60 * 60 * 24 * 365.25635)),
                isKid = true;


            if (birth >= 13) {
                // adult
                isKid = false;

            } else {
                // child
                isKid = true;
            }

            return isKid;
    },
    /**
        @public
        @description hasParent checks to see if the target element has a parent node.
        @param {el, cn} el = Object (DOM element), id = Number
    */
    hasParent: function (el, id) {
        if (el) {
            do {
                if (el.id === id) {
                    return true;
                }
                if (el.nodeType === 9) {
                    break;
                }
            }
            while((el = el.parentNode));
        }
        return false;
    }
  };
})();