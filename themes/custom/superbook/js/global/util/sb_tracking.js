/**
 * @file
 */

// global
var sb_tracking = {};
var sLanguage = null;

(function () {
	'use strict';
	// All the JavaScript for this file.
	sb_tracking.clickHit = function (id) {
		return alert(id);
	};

	sb_tracking.tracking = function () {

		sLanguage = dom.getLanguage();

		return true;
	};

	sb_tracking.actionEvent = function (sContentType, sTrackingValue) {

		sLanguage = dom.getLanguage();


		//console.log("sContentType: "+ sContentType + " sTrackingValue: " + sTrackingValue + " sLanguage: " + sLanguage);

		try{ omTrackSuperbook(sContentType, sTrackingValue, sLanguage); } catch(e){}
	};

	sb_tracking.link = function (sResourceName) {

		sLanguage = dom.getLanguage();

		//console.log("Track link " + sResourceName);

		try{ omTrackSuperbook(sResourceName, sLanguage); } catch(e){}

		return true;
	};

	sb_tracking.download = function (sResourceName) {
		sLanguage = dom.getLanguage();

		//console.log("Track link " + sResourceName);

		try{ omTrackSuperbookDownload(sResourceName, sLanguage); } catch(e){}

		return true;
	};

})();
