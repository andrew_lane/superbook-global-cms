(function buttons ($) {
  function init() {
    moreBtn();
  }
  function moreBtn () {
    var links = document.querySelectorAll('.btn-more');
    links.forEach(function (element) {
      var content = element.innerHTML;
      element.innerHTML = content  + ' <i class="material-icons">&#xE315;</i>';
    });
  }
  return init();
})($)
