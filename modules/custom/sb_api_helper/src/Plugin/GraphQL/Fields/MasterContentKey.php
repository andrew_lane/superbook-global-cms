<?php
namespace Drupal\sb_api_helper\Plugin\GraphQL\Fields;
//use Drupal\graphql_core\GraphQL\FieldPluginBase;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;
use Symfony\Component\HttpFoundation\Request;
use GraphQL\Type\Definition\ResolveInfo;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
/**
 * A simple field that returns the page title.
 *
 * For simplicity reasons, this example does not utilize dependency injection.
 *
 * @GraphQLField(
 *   id = "master_content_key",
 *   secure = true,
 *   type = "String",
 *   name = "masterContentKey",
 *   nullable = false,
 *   multi = false,
 *   parents = {"Entity","FieldNodeFieldRelatedQuestion",  "FieldNodeFieldBibleTopics",  "FieldNodeFieldVerseCfgTriviaQuestions",  "FieldNodeFieldBibleChapterVerses",  "FieldNodeFieldBibleBookChapters",  "FieldNodeFieldBibleVersionBooks",  "FieldNodeFieldBibleTimePeriod",  "FieldNodeFieldCharacterWordSearches",  "FieldNodeFieldCharacterWallpaper",  "FieldNodeFieldCharacterVideos",  "FieldNodeFieldEpisodeVideos",  "FieldNodeFieldEpisodeWallpapers",  "FieldNodeFieldEpisodeCharacters",  "FieldNodeFieldQuestionsAndAnswers","FieldNodeFieldDownloads","FieldNodeFieldWordSearches","FieldNodeFieldBadgeSource", "FieldNodeFieldMasterBadgeCriteria", "FieldNodeFieldBadgePrerequisite","FieldNodeFieldTrivia","FieldNodeFieldCharacterRelationships", "FieldNodeFieldRelatedEntity","FieldNodeFieldLocalizationStringList", "FieldNodeFieldPhrases"}
 * )
 */
class MasterContentKey extends FieldPluginBase {
  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    $val_class = get_class($value);
    $vid = 0;
    $nid = 0;
    if ($val_class == 'Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem'){
      $val = $value->getValue();
      $vid = $val["target_id"];
      $nid = $val["target_id"];//need to figure this out better

    }
    else{
      $vid_array = $value->get('vid')->getValue();
      $vid = $vid_array[0]["value"];
      $nid = $value->id();
    }
    $db = \Drupal\Core\Database\Database::getConnection();
    $query = $db->select('node__field_master_content_key',"mck")
    ->fields("mck",["field_master_content_key_value"])
    ->condition("revision_id",$vid);
    $results = $query->execute();
    $yield = false;
    foreach($results as $row){
      $yield = true;
      yield $row->field_master_content_key_value;
    }
    if(!$yield){
      $query = $db->select('node__field_master_content_key',"mck")
      ->fields("mck",["field_master_content_key_value"])
      ->condition("entity_id",$nid);
      $results = $query->execute();

      $last_entry = -1;
      foreach($results as $row){
        $yield = true;
        $last_entry = $row->field_master_content_key_value;

      }
      if ($yield){
        yield $last_entry;
      }
      else{
        echo "could not find key for $nid\n";
      }
    }
  }
}
