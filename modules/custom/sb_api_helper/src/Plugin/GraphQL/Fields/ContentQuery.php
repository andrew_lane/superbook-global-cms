<?php
namespace Drupal\sb_api_helper\Plugin\GraphQL\Fields;
//use Drupal\graphql_core\GraphQL\FieldPluginBase;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;
use Symfony\Component\HttpFoundation\Request;
use GraphQL\Type\Definition\ResolveInfo;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
/**
 * A simple field that returns the page title.
 *
 *
 * @GraphQLField(
 *   id = "content_query",
 *   secure = true,
 *   type = "Entity",
 *   name = "contentQuery",
 *   nullable = true,
 *   multi = true,
 *   arguments = {
 *     "type" = "String!",
 *     "limit" = "Int",
 *     "offset" = "Int"
 *   },
 *   parents = {"NodeContentVersion"}
 * )
 */
class ContentQuery extends FieldPluginBase {
  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    $limit = 10;
    $offset = 0;
    if (isset($value->dateRangeStart) && isset($value->dateRangeEnd)){
      if(isset($args['offset'])){
        $offset = intval($args['offset']);
      }
      if(isset($args['limit'])){
        $limit = intval($args['limit']);
      }
      $db = \Drupal\Core\Database\Database::getConnection();

      $query = $db->select("_node_revision","nr")
        ->fields('nr',['nid','vid'])
        ->condition("type",$args['type'])
        ->condition("revision_timestamp", $value->dateRangeStart, ">=")
        ->condition("revision_timestamp", $value->dateRangeEnd, "<=")
        ->range($offset,$limit);
      $results = $query->execute();
      $nodes = [];
      foreach($results as $row){
        $nodes[$row->nid] = $row->vid; //get the latest vid within the content push.
      }
      foreach($nodes as $nid=>$vid){
        //disabling revision loading because it is not working properly for some reason
        //may be safe to re-enable at a later date.
        //$node = node_revision_load($vid);
        $node = node_load($nid);
        yield $node;
      }
    }
  }
}
