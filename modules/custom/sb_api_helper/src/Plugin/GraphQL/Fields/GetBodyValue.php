<?php
namespace Drupal\sb_api_helper\Plugin\GraphQL\Fields;
//use Drupal\graphql_core\GraphQL\FieldPluginBase;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;
use Symfony\Component\HttpFoundation\Request;
use GraphQL\Type\Definition\ResolveInfo;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
/**
 * A simple field that returns the page title.
 *
 * For simplicity reasons, this example does not utilize dependency injection.
 *
 * @GraphQLField(
 *   id = "get_body_value",
 *   secure = true,
 *   type = "String",
 *   name = "getBodyValue",
 *   nullable = false,
 *   multi = false,
 *   parents = {"Entity"}
 * )
 */
class GetBodyValue extends FieldPluginBase {
  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value->hasField('body')){
      if (is_null($value->get('body')->value)){
        yield '';
      }
      else{
        yield $value->get('body')->value;
      }
    }
    else{
      yield '';
    }

  }
}
