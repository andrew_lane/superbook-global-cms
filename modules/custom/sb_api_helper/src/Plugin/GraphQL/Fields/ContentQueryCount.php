<?php
namespace Drupal\sb_api_helper\Plugin\GraphQL\Fields;
//use Drupal\graphql_core\GraphQL\FieldPluginBase;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;
use Symfony\Component\HttpFoundation\Request;
use GraphQL\Type\Definition\ResolveInfo;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
/**
 * A simple field that returns the page title.
 *
 *
 * @GraphQLField(
 *   id = "content_query_count",
 *   secure = true,
 *   type = "Int!",
 *   name = "contentQueryCount",
 *   nullable = true,
 *   multi = false,
 *   arguments = {
 *     "type" = "String!"
 *   },
 *   parents = {"NodeContentVersion"}
 * )
 */
class ContentQueryCount extends FieldPluginBase {
  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if (isset($value->dateRangeStart) && isset($value->dateRangeEnd)){
      $db = \Drupal\Core\Database\Database::getConnection();
      $query = $db->select("_node_revision","nr")
        ->condition("type",$args['type'])
        ->condition("revision_timestamp", $value->dateRangeStart, ">=")
        ->condition("revision_timestamp", $value->dateRangeEnd, "<=");
      $query->addExpression('COUNT(DISTINCT nid)','nidCount');
      $results = $query->execute();
      $recordCount = 0;
      foreach($results as $row){
        $recordCount = intval($row->nidCount);
      }
      yield $recordCount;
    }
    else{
      yield 0;
    }

  }
}
