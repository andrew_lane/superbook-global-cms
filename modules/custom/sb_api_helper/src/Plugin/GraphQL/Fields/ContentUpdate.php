<?php
namespace Drupal\sb_api_helper\Plugin\GraphQL\Fields;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;
use Symfony\Component\HttpFoundation\Request;
use GraphQL\Type\Definition\ResolveInfo;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
/**
 * A simple field that returns the page title.
 *
 * For simplicity reasons, this example does not utilize dependency injection.
 *
 * @GraphQLField(
 *   id = "content_update",
 *   secure = true,
 *   type = "NodeContentVersion",
 *   name = "contentUpdate",
 *   nullable = true,
 *   multi = false,
 *   arguments = {
 *     "fromRevision" = "Int",
 *   }
 * )
 */
class ContentUpdate extends FieldPluginBase{
  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    $start_timestamp = 0;
    $end_timestamp = 0;
    //main content node = 926102;

    $db = \Drupal\Core\Database\Database::getConnection();
    $cleanup_query = "
    insert into _node_revision
    select node_revision.*,(select type from node where node.nid = node_revision.nid) from node_revision
    where vid > (select max(vid) from _node_revision)";
    $db->query($cleanup_query)->execute();
    $results = $db->select("node_revision","nr")
      ->fields("nr",["vid","revision_timestamp"])
      ->condition("nid",926102)
      ->execute();
    foreach($results as $row){
      $int_timestamp = intval($row->revision_timestamp);
      //echo $int_timestamp;
      if(isset($args['fromRevision'])){
        if (intval($args['fromRevision']) == intval($row->vid)){
          $start_timestamp = $int_timestamp;
        }
      }
      $end_timestamp = $int_timestamp;
    }
    //echo "fail";

    $node = node_load(926102);

    $node->dateRangeStart = $start_timestamp;
    $node->dateRangeEnd = $end_timestamp;

    yield $node;

  }
}
