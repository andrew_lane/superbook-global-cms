<?php
namespace Drupal\sb_api_helper\Plugin\GraphQL\Fields;
//use Drupal\graphql_core\GraphQL\FieldPluginBase;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;
use Symfony\Component\HttpFoundation\Request;
use GraphQL\Type\Definition\ResolveInfo;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
/**
 * A simple field that returns the page title.
 *
 * For simplicity reasons, this example does not utilize dependency injection.
 *
 * @GraphQLField(
 *   id = "get_body_value",
 *   secure = true,
 *   type = "String",
 *   name = "getColorString",
 *   nullable = true,
 *   multi = false,
 *   arguments = {
 *     "field" = "String!"
 *   },
 *   parents = {"Entity"}
 * )
 */

class GetColorString extends FieldPluginBase {
  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if (isset($args["field"])){
      if ($value->hasField($args["field"])){
        $field_values = $value->get($args["field"])->getValue();
        foreach($field_values as $field_value){
          yield $field_value["color"];
        }
      }
    }
  }
}
