<?php
/**
 * @file
 * Contains \Drupal\sb_api_helper\Controller\DeleteController.
 */

namespace Drupal\sb_api_helper\Controller;

use Drupal\Core\Controller\ControllerBase;

class DeleteController extends ControllerBase {
  public function delete(){
    $batch_operations = [];
    for($i = 0; $i< 50; $i++){
      $batch_operations[] = array('\Drupal\sb_api_helper\Controller\DeleteController::deleteStuff', array());
    }
    $batch = array(
  			'title' => "Importing Episode Data",
  			'operations' => $batch_operations,
  			//'finished' => '\Drupal\sb_content\Controller\ContentController::finishBookSync',
  			//'file' => 'path_to_file_containing_myfunctions',
  		);
  		batch_set($batch);
  		// Only needed if not inside a form _submit handler.
  		// Setting redirect in batch_process.
  		return batch_process('/admin/content');
  }
  public static function deleteStuff(){
    $db = \Drupal\Core\Database\Database::getConnection();
    $query = $db->select("node","n")->fields("n",["nid"])->condition("type","contest")->range(0,100);
    $results = $query->execute();
    $ids = [];
    foreach($results as $row){
      $ids[$row->nid] = $row->nid;
    }
    entity_delete_multiple('node',$ids);
  }
}
