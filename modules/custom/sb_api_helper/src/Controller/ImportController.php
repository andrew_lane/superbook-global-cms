<?php
/**
 * @file
 * Contains \Drupal\sb_api_helper\Controller\ImportController.
 */

namespace Drupal\sb_api_helper\Controller;

use Drupal\Core\Controller\ControllerBase;

use Drupal\sb_api_helper\Utilities\Import\AppImages;
use Drupal\sb_api_helper\Utilities\Import\Avatar;
use Drupal\sb_api_helper\Utilities\Import\Badges;
use Drupal\sb_api_helper\Utilities\Import\Characters;
use Drupal\sb_api_helper\Utilities\Import\Episodes;
use Drupal\sb_api_helper\Utilities\Import\Games;
use Drupal\sb_api_helper\Utilities\Import\Misc;
use Drupal\sb_api_helper\Utilities\Import\Questions;
use Drupal\sb_api_helper\Utilities\Import\Topics;
use Drupal\sb_api_helper\Utilities\Import\Trivia;
use Drupal\sb_api_helper\Utilities\Import\Wallpapers;
use Drupal\sb_api_helper\Utilities\Import\WordSearches;


class ImportController extends ControllerBase {


	public function content2(){
		return array(
			'#type' => 'markup',
			'#markup' => "Finished it.",
		);
	}
	public function importGameStrings(){
		return Games::importGameStrings();
	}

	public function linkBadges(){
		return Badges::linkBadges();
	}
	public function importBadges(){
    return Badges::importBadges();
	}

	public function importGames($page){
		return Games::importGames($page);
	}
	public function importAvatarItems($index = 0){
		return Avatar::importAvatarItems($index);
	}
	public function importWallpapers () {
		return Wallpapers::importWallpapers();
	}
	public function importTopics(){
		return Topics::importTopics();
	}
	public function importWordSearches($index){
		return WordSearches::importWordSearches($index);
	}
  public function importQuestions($index){
		return Questions::importQuestions($index);
	}
  public function importTrivia($index){
		return Trivia::importTrivia($index);
	}
	public function importAppImages($index){
		return AppImages::importAppImages($index);
	}
	public function importCharacters(){
		return Characters::importCharacters();
	}
	public function fixCharacterWallpaperLinks(){
		return Characters::fixCharacterWallpaperLinks();
	}
	public function importEpisodes($index){
		return Episodes::importEpisodes($index);
	}
}
