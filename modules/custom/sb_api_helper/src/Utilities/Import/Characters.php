<?php
/**
 * @file
 * Contains \Drupal\sb_api_helper\Utilities\Import\Characters.
 */

namespace Drupal\sb_api_helper\Utilities\Import;
use Drupal\node\Entity\Node;
use Drupal\sb_api_helper\Utilities\Import\Misc;
class Characters{
  public static function importCharacters(){
    $import_data = file_get_contents("https://us-en.superbook.cbn.com/a/admin/export_characters");
    $characters = json_decode($import_data);
    $batch_operations = [];
    foreach($characters as $nid6=>$character){
      $batch_operations[] = array('\Drupal\sb_api_helper\Utilities\Import\Characters::batchImportCharacter', ["nid6"=>$nid6,"character"=>$character]);
    }
    foreach($characters as $nid6=>$character){
      $batch_operations[] = array('\Drupal\sb_api_helper\Utilities\Import\Characters::batchLinkCharacter', ["nid6"=>$nid6,"character"=>$character]);
    }
    $batch = [
			'title' => "Importing Character Data",
			'operations' => $batch_operations,
			//'finished' => '\Drupal\sb_content\Controller\ContentController::finishBookSync',
			//'file' => 'path_to_file_containing_myfunctions',
		];
		batch_set($batch);
  		// Only needed if not inside a form _submit handler.
  		// Setting redirect in batch_process.
		return batch_process('/admin/content');
  }
  public static function batchImportCharacter($nid6, $character){
    $db = \Drupal\Core\Database\Database::getConnection();
    $existing_nodes = [];
    $result = $db->select("node__field_nid6","n6")
      ->fields("n6",["entity_id","field_nid6_value"])
      ->execute();
    foreach($result as $row){
      $existing_nodes[$row->field_nid6_value] = $row->entity_id;
    }
    $node = null;
    if (isset($existing_nodes[$nid6])){
      $node = Node::load($existing_nodes[$nid6]);
    }
    else{
      if(!isset($character->translations->en->title)){
        return;
      }
      $node= Node::create([
        'type'        => 'character',
        'title'       => $character->translations->en->title,
        'field_nid6' => $nid6,
        'field_master_content_key' => $nid6,
        'langcode' => 'en'
      ]);
    }
    //do untranslated stuff
    $node->set('field_character_type',(string) $character->character_type);
    $node->set('field_character_core_passage',(string) $character->core_passage_reference[0]);
    $reference_groups = $character->bible_references;
    $individual_references = [];
    foreach($reference_groups as $group){
      $parts = explode(".",$group);
      if (count($parts) == 3){
        $verses = $parts[2];
        if (strpos($verses,",")>0){
          $subgroups = explode(",",$verses);
          foreach($subgroups as $subgroup){
            if(strpos($subgroup,"-")>0){
              $range = explode("-",$subgroup);
              for($i = $range[0]; $i<= $range[1]; $i++){
                $passage = $parts[0].".".$parts[1].".".$i;
                $individual_references[$passage] = $passage;
              }
            }
            else{
              $passage = $parts[0].".".$parts[1].".".$subgroup;
              $individual_references[$passage] = $passage;
            }
          }
        }
        elseif(strpos($verses,"-")>0){
          $range = explode("-",$verses);
          for($i = $range[0]; $i<= $range[1]; $i++){
            $passage = $parts[0].".".$parts[1].".".$i;
            $individual_references[$passage] = $passage;
          }
        }
        else{
          $individual_references[$group] = $group;
        }
      }
      else{
        $individual_references[$group] = $group;
      }
    }
    $node->set('field_bible_references',$individual_references);
    $wallpaper_targets = [];
    foreach($character->wallpapers as $wallpaper_nid6){
      if (isset($existing_nodes[$wallpaper_nid6])){
        $wallpaper_targets[] = $existing_nodes[$wallpaper_nid6];
      }
      else{
        if(intval($wallpaper_nid6) != 13588071){//this wallpaper did not get imported because it has no title
          die("Wallpaper $wallpaper_nid6 is missing - import wallpapers before characters");
        }
      }
    }
    $node->set('field_character_wallpaper',$wallpaper_targets);

    $video_clip_targets = [];
    foreach($character->videos as $video_nid6){
      if (isset($existing_nodes[$video_nid6])){
        $video_clip_targets[] = $existing_nodes[$video_nid6];
      }
      else{
        die("video $video_nid6 is missing - import videos before characters");
      }
    }
    $node->set('field_character_videos',$video_clip_targets);




    $qa_targets = [];
    foreach($character->qa as $qa_nid6){
      if (isset($existing_nodes[$qa_nid6])){
        $qa_targets[] = $existing_nodes[$qa_nid6];
      }
      else{
        die("Q&A $qa_nid6 is missing - import Q&A before characters");
      }
    }
    $node->set('field_questions_and_answers',$qa_targets);

    $trivia_targets = [];
    foreach($character->trivia as $trivia_nid6){
      if (isset($existing_nodes[$trivia_nid6])){
        $trivia_targets[] = $existing_nodes[$trivia_nid6];
      }
      else{
        die("Trivia Question $qa_nid6 is missing - import Trivia before characters");
      }
    }
    $node->set('field_trivia',$trivia_targets);

    $ws_targets = [];
    foreach($character->word_searches as $ws_nid6){
      if (isset($existing_nodes[$ws_nid6])){
        $ws_targets[] = $existing_nodes[$ws_nid6];
      }
    }
    $node->set('field_word_searches',$ws_targets);

    $topic_targets = [];
    foreach($character->tags as $tag_nid6){
      if (isset($existing_nodes[$tag_nid6])){
        $topic_targets[] = $existing_nodes[$tag_nid6];
      }
      else{
        die("Tag/Topic $tag_nid6 is missing - import Tags/Topics before characters");
      }
    }
    $node->set('field_bible_topics',$topic_targets);

    if(isset($character->bible_time_reference) && $character->bible_time_reference != null){
      $time_periods = [];
      foreach($character->bible_time_reference as $time_period_nid){
        if (isset($existing_nodes[$time_period_nid])){
          $time_periods = $existing_nodes[$time_period_nid];
        }
        else{
          die("invalid time period: $time_period_nid - import time periods before characters");
        }
      }
      $node->set('field_bible_time_period',$time_periods);

    }


    $source_field = "eg_image";
    $dst = "images/characters/bible_landing/";
    $node_field = "field_bible_landing_image";
    if ($character->$source_field != null){
      $source = $character->$source_field;
      $image_file_data = file_get_contents('https://us-en.superbook.cbn.com/'.$source);
      $ext = ".png";
      $last_dot = mb_strrpos($source,".");
      if ($last_dot > 0 ){
        $ext = mb_substr($source,$last_dot, strlen($source));
      }
      $last_slash = mb_strrpos($source,"/");
      $filename = $nid6."_".mb_substr($source,$last_slash, strlen($source));
      $filename = str_replace($ext,"",$filename);
      $filename = Misc::cleanFileName($filename);
      $filename .= $ext;
      $new_path = 'public://'.$dst. $filename;
      $new_image_file = file_save_data($image_file_data, $new_path, FILE_EXISTS_REPLACE);
      $node->set($node_field,["target_id"=>$new_image_file->id()]);
    }
    $source_field = "static_image";
    $dst = "images/characters/static/";
    $node_field = "field_character_static_image";
    if ($character->$source_field != null){
      $source = $character->$source_field;
      $image_file_data = file_get_contents('https://us-en.superbook.cbn.com/'.$source);
      $ext = ".png";
      $last_dot = mb_strrpos($source,".");
      if ($last_dot > 0 ){
        $ext = mb_substr($source,$last_dot, strlen($source));
      }
      $last_slash = mb_strrpos($source,"/");
      $filename = $nid6."_".mb_substr($source,$last_slash, strlen($source));
      $filename = str_replace($ext,"",$filename);
      $filename = Misc::cleanFileName($filename);
      $filename .= $ext;
      $new_path = 'public://'.$dst. $filename;
      $new_image_file = file_save_data($image_file_data, $new_path, FILE_EXISTS_REPLACE);
      $node->set($node_field,["target_id"=>$new_image_file->id()]);
    }
    $source_field = "dynamic_image";
    $dst = "images/characters/dynamic/";
    $node_field = "field_character_dynamic_image";
    if ($character->$source_field != null){
      $source = $character->$source_field;
      $image_file_data = file_get_contents('https://us-en.superbook.cbn.com/'.$source);
      $ext = ".png";
      $last_dot = mb_strrpos($source,".");
      if ($last_dot > 0 ){
        $ext = mb_substr($source,$last_dot, strlen($source));
      }
      $last_slash = mb_strrpos($source,"/");
      $filename = $nid6."_".mb_substr($source,$last_slash, strlen($source));
      $filename = str_replace($ext,"",$filename);
      $filename = Misc::cleanFileName($filename);
      $filename .= $ext;
      $new_path = 'public://'.$dst. $filename;
      $new_image_file = file_save_data($image_file_data, $new_path, FILE_EXISTS_REPLACE);
      $node->set($node_field,["target_id"=>$new_image_file->id()]);
    }
    if ($character->bible_book_reference != null){
      $book_references = [];
      foreach($character->bible_book_reference as $reference){
        $bible_book_reference = Misc::bibleBookFromNid6($reference);
        if ($bible_book_reference === false){
          die("failed to determine book reference from ".$reference);
        }
        $book_references[] = $bible_book_reference;
      }

      $node->set('field_bible_book_reference',$book_references);
    }
    $node->save();

/*
[app_images] => 313306 //app images are apparently done after characters, not before.
//profile header image and browse image are from app images
*/
    foreach($character->translations as $language=>$translation){
      //need to add ms(MALAY)
      //invalid languages included in d6 source: hk-en, ng-en, in-en, (backwards)
      if (($language != "hk-en")&&($language != "ng-en")&&($language != "in-en")&&($language != "en-ie")&&($language != "en-id")&&($language != "it")&&($language != "tl")&&($language != "en-ke")&&($language != "ms")&&($language != "en-my")&&($language != "en-sg")) {
        if($node->hasTranslation($language)){
          $node_translation = $node->getTranslation($language);
        }
        else{
          $node_translation = $node->addTranslation($language);
        }
        if(!isset($translation->title)){
          return;
        }
        $node_translation->set("title",(string)$translation->title);
        $node_translation->set("field_short_title",(string)$translation->short_title);
        $node_translation->set("field_character_infoscan",(string)$translation->infoscan);
        $node_translation->set("field_character_bible_infoscan",(string)$translation->bible_infoscan);
        $node_translation->set("field_character_intl_infoscan",(string)$translation->intl_infoscan);
        $node_translation->set("field_character_life_lessons",$translation->life_lessons);
        $node_translation->set("field_age_rating",(string)$translation->age_rating);
        if($translation->ready_for_app == 1){
          $node_translation->set("field_ready_for_app",1);
        }
        else{
          $node_translation->set("field_ready_for_app",0);
        }
        if($translation->simplified_site == 1){
          $node_translation->set("field_simplified_site",1);
        }
        else{
          $node_translation->set("field_simplified_site",0);
        }
        if($translation->status == 1){
          $node_translation->setPublished(true);
        }
        else{
          $node_translation->setPublished(false);
        }
        $node_translation->save();
      }

    }
  }
  public static function batchLinkCharacter($nid6, $character){
    $db = \Drupal\Core\Database\Database::getConnection();
    $existing_nodes = [];
    $result = $db->select("node__field_nid6","n6")
      ->fields("n6",["entity_id","field_nid6_value"])
      ->execute();
    foreach($result as $row){
      $existing_nodes[$row->field_nid6_value] = $row->entity_id;
    }
    if (
      (intval($nid6) == 13201361)
      ||(intval($nid6)==13175376)
      ||(intval($nid6)==13175381)
      ||(intval($nid6)==13175386)
      ||(intval($nid6)==13175491)
      ||(intval($nid6)==13175496)
      ||(intval($nid6)==13176221)
      ||(intval($nid6)==13176586)
      ||(intval($nid6)==13176591)
      ||(intval($nid6)==13176731)
      ||(intval($nid6)==13176776)
    ){
      //ignore linking for certain profiles which do not exist in English
      return;
    }

    $node = Node::load($existing_nodes[$nid6]);
    $rcp_targets = [];
    foreach($character->related_char_profiles as $cp_nid6){
      $rcp_targets[] = $existing_nodes[$cp_nid6];
    }
    $node->set('field_character_relationships',$rcp_targets);
    $node->save();
  }
  public static function fixCharacterWallpaperLinks(){
    \set_time_limit(36000);
    ini_set('memory_limit','256M');
    $existing_nodes = [];
    $result = db_query("select entity_id, field_nid6_value from node__field_nid6");
    $data = $result->fetchAll();
    foreach($data as $datum){
      $existing_nodes[$datum->field_nid6_value] = $datum->entity_id;
    }
    $query = "select node.nid, content_field_cp_image_wallpaper.delta, content_field_cp_image_wallpaper.field_cp_image_wallpaper_nid from al_superbook_6.node
    inner join al_superbook_6.content_field_cp_image_wallpaper on node.vid = content_field_cp_image_wallpaper.vid
    where type = 'char_profile' and language = 'en'
    and field_cp_image_wallpaper_nid is not null
    order by node.nid, content_field_cp_image_wallpaper.delta";
    $result = db_query($query);
    $original_mapping = [];
    $missing_wallpapers = [];
    foreach($result as $row){
      if ((isset($existing_nodes[$row->nid])) && ((isset($existing_nodes[$row->field_cp_image_wallpaper_nid])))){
        $original_mapping[$existing_nodes[$row->nid]][] = ["target_id"=>$existing_nodes[$row->field_cp_image_wallpaper_nid]];
      }
      else{
        if (!isset($existing_nodes[$row->nid])){
          //echo "missing character node $row->nid\n";
        }
        else{
          $missing_wallpapers[] = $row->field_cp_image_wallpaper_nid;
          //echo "missing wallpaper node $row->field_cp_image_wallpaper_nid\n";
        }
      }
    }
    if(count($missing_wallpapers)> 0){
      echo "could not fix unlinked wallpapers because some are missing.  Importing missing wallpapers instead.  Rerun this script to link.\n";
      //print_r($missing_wallpapers);
      $this->importWallpapers();
      exit;
    }
    foreach($original_mapping as $character_nid=>$wallpaper_set){
      $character = node_load($character_nid);
      $character->set('field_character_wallpaper',$wallpaper_set);
      $character->save();
    }
    echo "complete";
    exit;
  }
}
