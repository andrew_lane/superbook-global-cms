<?php
/**
 * @file
 * Contains \Drupal\sb_api_helper\Utilities\Import\Videos.
 */

namespace Drupal\sb_api_helper\Utilities\Import;
use Drupal\node\Entity\Node;
use Drupal\sb_api_helper\Utilities\Import\Misc;

class Videos{
  public static function importVideos(){
    $import_data = file_get_contents("https://us-en.superbook.cbn.com/a/admin/export_videos");
    $videos = json_decode($import_data);
    $batch_operations = [];
    foreach($videos as $nid6=>$video){
      $batch_operations[] = array('\Drupal\sb_api_helper\Utilities\Import\Videos::batchImportVideo', ["nid6"=>$nid6,"video"=>$video]);
    }
    $batch = [
			'title' => "Importing Video Data",
			'operations' => $batch_operations,
			//'finished' => '\Drupal\sb_content\Controller\ContentController::finishBookSync',
			//'file' => 'path_to_file_containing_myfunctions',
		];
		batch_set($batch);
  		// Only needed if not inside a form _submit handler.
  		// Setting redirect in batch_process.
		return batch_process('/admin/content');
  }
  public static function batchImportVideo($nid6, $video){
    $db = \Drupal\Core\Database\Database::getConnection();
    $existing_nodes = [];
    $result = $db->select("node__field_nid6","n6")
      ->fields("n6",["entity_id","field_nid6_value"])
      ->execute();
    foreach($result as $row){
      $existing_nodes[$row->field_nid6_value] = $row->entity_id;
    }
    $node = null;
    if (!isset($video->translations->en->title)){
      return;
    }
    if (isset($existing_nodes[$nid6])){
      $node = Node::load($existing_nodes[$nid6]);
    }
    else{
      $node= Node::create([
        'type'        => 'video',
        'title'       => $video->translations->en->title,
        'field_nid6' => $nid6,
        'field_master_content_key' => $nid6,
        'langcode' => 'en'
      ]);
    }
    //do untranslated stuff


    $node->set('field_video_type','teaser-clip');

    $reference_groups = $video->bible_references;
    $individual_references = [];
    foreach($reference_groups as $group){
      $parts = explode(".",$group);
      if (count($parts) == 3){
        $verses = $parts[2];
        if (strpos($verses,",")>0){
          $subgroups = explode(",",$verses);
          foreach($subgroups as $subgroup){
            if(strpos($subgroup,"-")>0){
              $range = explode("-",$subgroup);
              for($i = $range[0]; $i<= $range[1]; $i++){
                $passage = $parts[0].".".$parts[1].".".$i;
                $individual_references[$passage] = $passage;
              }
            }
            else{
              $passage = $parts[0].".".$parts[1].".".$subgroup;
              $individual_references[$passage] = $passage;
            }
          }
        }
        elseif(strpos($verses,"-")>0){
          $range = explode("-",$verses);
          for($i = $range[0]; $i<= $range[1]; $i++){
            $passage = $parts[0].".".$parts[1].".".$i;
            $individual_references[$passage] = $passage;
          }
        }
        else{
          $individual_references[$group] = $group;
        }
      }
      else{
        $individual_references[$group] = $group;
      }
    }
    $node->set('field_bible_references',$individual_references);

    $topic_targets = [];
    foreach($video->tags as $tag_nid6){
      if (isset($existing_nodes[$tag_nid6])){
        $topic_targets[] = $existing_nodes[$tag_nid6];
      }
      else{
        die("Tag/Topic $tag_nid6 is missing - import Tags/Topics before characters");
      }
    }
    $node->set('field_bible_topics',$topic_targets);

    if(isset($video->time_periods) && $video->time_periods != null){
      $time_periods = [];
      foreach($video->time_periods as $time_period_nid){
        if (isset($existing_nodes[$time_period_nid])){
          $time_periods = $existing_nodes[$time_period_nid];
        }
        else{
          die("invalid time period: $video->bible_time_reference - import time periods before characters");
        }
      }
      $node->set('field_bible_time_period',$time_periods);

    }


    $source_field = "570x320_image";
    $dst = "images/video/";
    $node_field = "field_video_image";
    if ($video->$source_field != null){
      $source = $video->$source_field;
      $image_file_data = file_get_contents('https://us-en.superbook.cbn.com/'.$source);
      $ext = ".png";
      $last_dot = mb_strrpos($source,".");
      if ($last_dot > 0 ){
        $ext = mb_substr($source,$last_dot, strlen($source));
      }
      $last_slash = mb_strrpos($source,"/");
      $filename = $nid6."_".mb_substr($source,$last_slash, strlen($source));
      $filename = str_replace($ext,"",$filename);
      $filename = Misc::cleanFileName($filename);
      $filename .= $ext;
      $new_path = 'public://'.$dst. $filename;
      $new_image_file = file_save_data($image_file_data, $new_path, FILE_EXISTS_REPLACE);
      $node->set($node_field,["target_id"=>$new_image_file->id()]);
    }

    if ($video->book_references != null){
      $book_references = [];
      foreach($video->book_references as $reference){
        $bible_book_reference = Misc::bibleBookFromNid6($reference);
        if ($bible_book_reference === false){
          die("failed to determine book reference from ".$reference);
        }
        $book_references[] = $bible_book_reference;
      }

      $node->set('field_bible_book_reference',$book_references);
    }
    $node->save();

/*
[app_images] => 313306 //app images are apparently done after characters, not before.
//profile header image and browse image are from app images
*/
    foreach($video->translations as $language=>$translation){
      //need to add ms(MALAY)
      //invalid languages included in d6 source: hk-en, ng-en, in-en, (backwards)
      if (isset($translation->title)&&($language != "hk-en")&&($language != "ng-en")&&($language != "in-en")&&($language != "en-ie")&&($language != "en-id")&&($language != "it")&&($language != "tl")&&($language != "en-ke")&&($language != "ms")&&($language != "en-my")&&($language != "en-sg")) {
        if($node->hasTranslation($language)){
          $node_translation = $node->getTranslation($language);
        }
        else{
          $node_translation = $node->addTranslation($language);
        }
        $node_translation->set("title",(string)$translation->title);
        $node_translation->set("field_short_title",(string)$translation->short_title);
        $node_translation->set("field_brightcove_video_id",(string)$translation->brightcove_video_id);

        if($translation->ready_for_app == 1){
          $node_translation->set("field_ready_for_app",1);
        }
        else{
          $node_translation->set("field_ready_for_app",0);
        }
        if($translation->simplified_site == 1){
          $node_translation->set("field_simplified_site",1);
        }
        else{
          $node_translation->set("field_simplified_site",0);
        }
        if($translation->status == 1){
          $node_translation->setPublished(true);
        }
        else{
          $node_translation->setPublished(false);
        }
        $node_translation->save();
      }

    }
  }
}
