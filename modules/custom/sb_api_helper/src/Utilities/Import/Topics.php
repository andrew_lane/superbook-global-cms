<?php
/**
 * @file
 * Contains \Drupal\sb_api_helper\Utilities\Import\Topics.
 */

namespace Drupal\sb_api_helper\Utilities\Import;
use Drupal\node\Entity\Node;
class Topics{
  public static function importTopics(){
    \set_time_limit(36000);
    ini_set('memory_limit','384M');
    $nodes_saved = 0;
    $ignored_nodes = 0;
    $invalid_nodes = [];
    $import_data = file_get_contents("https://us-en.superbook.cbn.com/c/admin/topic_export");
    $start_time = \microtime(true);
    $json = json_decode($import_data);
    $batch_operations = [];
    foreach($json as $nid6=>$topic){
      $batch_operations[] = array('\Drupal\sb_api_helper\Utilities\Import\Characters::batchImportTopic', ["nid6"=>$nid6,"topic"=>$topic]);
    }
    $batch = [
			'title' => "Importing Bible Topics",
			'operations' => $batch_operations,
			//'finished' => '\Drupal\sb_content\Controller\ContentController::finishBookSync',
			//'file' => 'path_to_file_containing_myfunctions',
		];
		batch_set($batch);
    return batch_process('/admin/content');
  }
  public static function batchImportTopic($nid6,$topic){
    $iterations = 0;
    $db = \Drupal\Core\Database\Database::getConnection();
    $existing_nodes = [];
    $result = $db->select("node__field_nid6","n6")
      ->fields("n6",["entity_id","field_nid6_value"])
      ->execute();
    foreach($result as $row){
      $existing_nodes[$row->field_nid6_value] = $row->entity_id;
    }
    $iterations++;
    $valid_node = false;
    $topic_title = "unknown";
    if (isset($topic->translations->en->title)){
      $topic_title = $topic->translations->en->title;
    }
    elseif (isset($topic->translations->zxx->title)){
      $topic_title = $topic->translations->zxx->title;
    }
    else{
      //echo "can't handle topic: $nid6";
      //print_r($topic);
      //exit;
    }
    if($topic_title == "unknown"){

    }
    if (isset($existing_nodes[$nid6])){
      $node_has_changes = false;
      //saving is VERY expensive in this script, so we want to go through every effort to determine
      //if the node even needs to be saved.
      $node = node_load($existing_nodes[$nid6]);
      if (!$node->hasTranslation("en")){
        $node = $node->addTranslation("en");
        $node_has_changes = true;
      }
      $existing_title = $node->get('title')->value;

      if($existing_title != $topic_title){
        //echo "[$existing_title] is not the same as [$topic->translations->en->title]\n";
        $node_has_changes = true;
      }
      $existing_references = $node->get('field_bible_references')->getValue();
      $imported_count = count($topic->references);
      $existing_count = count($existing_references);
      if($imported_count != $existing_count){
        $node_has_changes = true;
      }

      for($i = 0; $i< $imported_count; $i++){
        if ($existing_references[$i]["value"] != $topic->references[$i]){
          //echo $existing_references[$i]["value"]." is not the same as ";
          //echo $topic->references[$i]."\n\n";
          $node_has_changes = true;
        }
      }
      if($node_has_changes){
        //error_log("changing title to ".$topic->translations->en->title. "for $nid6");
        $node->set('title', $topic_title);
        $node->set('field_bible_references',$topic->references);
        $nodes_saved++;
        $node->save();
      }
      else{
        $ignored_nodes++;
      }

      $valid_node = true;
    }
    else{
      if ($topic_title != "unknown" && $topic_title != null && $topic_title != ""){
        //error_log("creating new node with title ".$topic->translations->en->title. "for $nid6");
        $node = Node::create([
          'type'        => 'bible_topic',
          'title'       => $topic_title,
          'field_nid6' => $nid6,
          'field_master_content_key'=>$nid6,
          'langcode' => 'en'
        ]);
        $nodes_saved++;
        $node->save();
        $valid_node = true;
      }
    }
    if($valid_node){
      foreach($topic->translations as $language=>$translation){
        $translation_changed = false;
        if (isset($translation->title) && $translation->title != null && $translation->title != ""){
          if($language != "en"){
            if($language != "tl"){
              if($language != "it"){
                if($language != "et"){
                  if($language != "hy"){
                    if($language != "bn"){
                      if($language != "ta"){
                        if($language != "te"){
                          if($language != "en-ie"){
                            if($language != "en-id"){
                              if($language != "en-ke"){
                                if($language != "zxx"){
                                  if (!$node->hasTranslation($language)){
                                    $translated_node = $node->addTranslation($language);
                                    $translation_changed = true;
                                  }
                                  $translated_node = $node->getTranslation($language);
                                  if ($translated_node->get('title')->value != $translation->title){
                                    //error_log("changing title to ".$translation->title. "for $nid6,$language");
                                    $translated_node->set('title', $translation->title);
                                    $translation_changed = true;
                                  }
                                  else{
                                    $ignored_nodes++;
                                  }
                                  if ($translation_changed){
                                    $nodes_saved++;
                                    $translated_node->save();
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    else{
      $invalid_nodes[] = $nid6;
    }
  }
}
