<?php
/**
 * @file
 * Contains \Drupal\sb_api_helper\Utilities\Import\Episodes.
 */

namespace Drupal\sb_api_helper\Utilities\Import;
use Drupal\node\Entity\Node;
class Episodes{
  public static function importEpisodes(){
    //$import_data = file_get_contents("https://dev-superbookd6.pantheonsite.io/a/admin/export_episodes");
    $import_data = file_get_contents("https://us-en.superbook.cbn.com/a/admin/export_episodes");

    $episodes = json_decode($import_data);

    $batch_operations = [];
    foreach($episodes as $nid6=>$episode){
      $batch_operations[] = array('\Drupal\sb_api_helper\Utilities\Import\Episodes::batchImportEpisode', ["nid6"=>$nid6,"episode"=>$episode]);
    }
    $batch = [
      'title' => "Importing Episodes",
      'operations' => $batch_operations,
      //'finished' => '\Drupal\sb_content\Controller\ContentController::finishBookSync',
      //'file' => 'path_to_file_containing_myfunctions',
    ];
    batch_set($batch);
      // Only needed if not inside a form _submit handler.
      // Setting redirect in batch_process.
    return batch_process('/admin/content');
  }
  public static function batchImportEpisode($nid6,$episode){
    if(!isset($episode->translations->en->title)){
      return;
    }
    $db = \Drupal\Core\Database\Database::getConnection();
    $existing_nodes = [];
    $result = $db->select("node__field_nid6","n6")
      ->fields("n6",["entity_id","field_nid6_value"])
      ->execute();
    foreach($result as $row){
      $existing_nodes[$row->field_nid6_value] = $row->entity_id;
    }
    $node = null;
    if (isset($existing_nodes[$nid6])){
      $node = Node::load($existing_nodes[$nid6]);
    }
    else{
      $node= Node::create([
        'type'        => 'episode',
        'title'       => $episode->translations->en->title,
        'field_nid6' => $nid6,
        'field_master_content_key' => $nid6,
        'langcode' => 'en'
      ]);
    }
    $reference_groups = $episode->bible_references;
    $individual_references = [];
    foreach($reference_groups as $group){
      $parts = explode(".",$group);
      if (count($parts) == 3){
        $verses = $parts[2];
        if (strpos($verses,",")>0){
          $subgroups = explode(",",$verses);
          foreach($subgroups as $subgroup){
            if(strpos($subgroup,"-")>0){
              $range = explode("-",$subgroup);
              for($i = $range[0]; $i<= $range[1]; $i++){
                $passage = $parts[0].".".$parts[1].".".$i;
                $individual_references[$passage] = $passage;
              }
            }
            else{
              $passage = $parts[0].".".$parts[1].".".$subgroup;
              $individual_references[$passage] = $passage;
            }
          }
        }
        elseif(strpos($verses,"-")>0){
          $range = explode("-",$verses);
          for($i = $range[0]; $i<= $range[1]; $i++){
            $passage = $parts[0].".".$parts[1].".".$i;
            $individual_references[$passage] = $passage;
          }
        }
        else{
          $individual_references[$group] = $group;
        }
      }
      else{
        $individual_references[$group] = $group;
      }
    }
    $node->set('field_bible_references',$individual_references);

    $wallpaper_targets = [];
    foreach($episode->wallpapers as $wallpaper_nid6){
      if (isset($existing_nodes[$wallpaper_nid6])){
        $wallpaper_targets[] = $existing_nodes[$wallpaper_nid6];
      }
      else{
        if(intval($wallpaper_nid6) != 13588071){//this wallpaper did not get imported because it has no title
          die("Wallpaper $wallpaper_nid6 is missing - import wallpapers before episodes");
        }
      }
    }
    $node->set('field_episode_wallpapers',$wallpaper_targets);

    $ws_targets = [];
    foreach($episode->word_searches as $ws_nid6){
      if (isset($existing_nodes[$ws_nid6])){
        $ws_targets[] = $existing_nodes[$ws_nid6];
      }
    }
    $node->set('field_word_searches',$ws_targets);

    $qa_targets = [];
    foreach($episode->qa as $qa_nid6){
      if (isset($existing_nodes[$qa_nid6])){
        $qa_targets[] = $existing_nodes[$qa_nid6];
      }
      else{
        die("Q&A $qa_nid6 is missing - import Q&A before episodes");
      }
    }
    $node->set('field_questions_and_answers',$qa_targets);

    $video_clip_targets = [];
    foreach($episode->videos as $video_nid6){
      if (isset($existing_nodes[$video_nid6])){
        $video_clip_targets[] = $existing_nodes[$video_nid6];
      }
      else{
        die("video $video_nid6 is missing - import videos before episodes");
      }
    }
    $node->set('field_episode_videos',$video_clip_targets);

    $related_char_profiles = [];
    foreach($episode->related_char_profiles as $char_nid6){
      if (isset($existing_nodes[$char_nid6])){
        $related_char_profiles[] = $existing_nodes[$char_nid6];
      }
      else{
        die("character $char_nid6 is missing - import character profiles before episodes");
      }
    }
    $node->set('field_episode_characters',$related_char_profiles);
    //untranslated
    Misc::downloadImageField($episode,"eg_background_image","images/episodes/background/","field_episode_background_image",$node);
    Misc::downloadImageField($episode,"eg_large_teaser_image","images/episodes/teaser/","field_large_teaser_image",$node);
    Misc::downloadImageField($episode,"eg_preview_image_430_240","images/episodes/preview/","field_episode_preview_image",$node);

    $base_downloads = [];
    foreach($episode->downloadable_resources as $download_nid6){
      if (isset($existing_nodes[$download_nid6])){
        $base_downloads[] = $existing_nodes[$download_nid6];
      }
      else{
        die("invalid download nid6: $download_nid6. import downloadable resources before episodes");
      }
    }
    $node->set('field_downloads',$base_downloads);

    $node->save();
    //not imported: $episode->eg_qa_image, eg_video_image, eg_bible_char_profiles_image, eg_char_profiles_image

    //translated
    foreach($episode->translations as $language=>$translation){
      if (($language != "hk-en")&&($language != "ng-en")&&($language != "in-en")&&($language != "tl")&&($language != "ms")) {
        if($node->hasTranslation($language)){
          $node_translation = $node->getTranslation($language);
        }
        else{
          $node_translation = $node->addTranslation($language);
        }
        if(!isset($translation->title)){
          return;
        }
        $node_translation->set('title', (string)$translation->title);
        $node_translation->set('field_adventure_guide_intro', (string)$translation->adv_overview);
        $node_translation->set('field_episode_bible_synopsis', (string)$translation->bible_synopsis);
        $node_translation->set('field_episode_characters_intro', (string)$translation->cp_overview);
        $node_translation->set('field_episode_number', (string)$translation->episode_number);
        $node_translation->set('field_episode_synopsis', (string)$translation->synopsis);
        $node_translation->set('field_episode_take_away', (string)$translation->take_away);
        $node_translation->set('field_episode_video_intro', (string)$translation->video_overview);
        $node_translation->set('field_season', (string)$translation->season_number);
        Misc::downloadImageField($translation,"eg_adv_guide_preview_image","images/episodes/adventure_guide/","field_adventure_guide_image",$node_translation);
        if($translation->simplified_site == 1){
          $node_translation->set("field_simplified_site",1);
        }
        else{
          $node_translation->set("field_simplified_site",0);
        }
        if($translation->status == 1){
          $node_translation->setPublished(true);
        }
        else{
          $node_translation->setPublished(false);
        }

        $local_downloads = $base_downloads;
        foreach($translation->local_downloadable_resources as $download_nid6){
          if (isset($existing_nodes[$download_nid6])){
            $already_included = false;
            foreach($local_downloads as $dlnid6){
              if (intval($dlnid6) == intval($download_nid6)){
                $already_included = true;
              }
            }
            if(!$already_included){
              $local_downloads[] = $existing_nodes[$download_nid6];
            }
          }
        }

        $node_translation->set('field_downloads',$local_downloads);
      }

      $node_translation->save();
    }
  }
}
