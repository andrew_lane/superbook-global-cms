<?php
/**
 * @file
 * Contains \Drupal\sb_api_helper\Utilities\Import\DownloadableResources.
 */

namespace Drupal\sb_api_helper\Utilities\Import;
use Drupal\node\Entity\Node;
class DownloadableResources{
  public static function importDownloadableResources(){
    $import_data = file_get_contents("https://us-en.superbook.cbn.com/a/admin/export_downloads");
    $downloads = json_decode($import_data);
    $db = \Drupal\Core\Database\Database::getConnection();
    $existing_nodes = [];
    $result = $db->select("node__field_nid6","n6")
      ->fields("n6",["entity_id","field_nid6_value"])
      ->execute();
    foreach($result as $row){
      $existing_nodes[$row->field_nid6_value] = $row->entity_id;
    }


    $batch_operations = [];
    foreach($downloads as $nid6=>$download){
      $node = null;
      if (isset($existing_nodes[$nid6])){
        $node = Node::load($existing_nodes[$nid6]);
      }
      else{
        $node= Node::create([
          'type'        => 'downloadable_resource',
          'title'       => "Unavailable in English",
          'field_nid6' => $nid6,
          'field_master_content_key' => $nid6,
          'langcode' => 'en'
        ]);
        $node->setPublished(false);
      }
      $node->save();
      foreach($download->translations as $langcode=>$translation){
        $batch_operations[] = array('\Drupal\sb_api_helper\Utilities\Import\DownloadableResources::batchImportDownloadableResource', ["nid6"=>$nid6,"entity_id"=>$node->id(),"langcode"=>$langcode,"translation"=>$translation]);
      }

    }
    $batch = [
			'title' => "Importing DownloadableResources",
			'operations' => $batch_operations,
			//'finished' => '\Drupal\sb_content\Controller\ContentController::finishBookSync',
			//'file' => 'path_to_file_containing_myfunctions',
		];
		batch_set($batch);
  		// Only needed if not inside a form _submit handler.
  		// Setting redirect in batch_process.
		return batch_process('/admin/content');
  }
  public static function batchImportDownloadableResource($nid6,$entity_id,$langcode,$translation){
    $node = Node::load($entity_id);
    if ($node == false){
      return;
    }
    if($node->getType()=="video"){
      echo $node->id();
      exit;
    }

    if((trim($langcode) != "")&&($langcode != "ms")&&($langcode != "hk-en")&&($langcode != "ng-en")&&($langcode != "tl")&&($langcode != "in-en")){
      $translated_node = null;
      if ($node->hasTranslation($langcode)){
        $translated_node = $node->getTranslation($langcode);
      }
      else{
        $translated_node = $node->addTranslation($langcode);
      }
      $translated_node->set('title',$translation->title);
      $translated_node->set('field_download_description',$translation->body);
      $source_field = "file";
      $dst = "downloadable_resources/";
      $node_field = "field_resource";
      if ($translation->$source_field != null){
        $source = $translation->$source_field;
        $image_file_data = file_get_contents('https://us-en.superbook.cbn.com/'.$source);
        $ext = ".png";
        $last_dot = mb_strrpos($source,".");
        if ($last_dot > 0 ){
          $ext = mb_substr($source,$last_dot, strlen($source));
        }
        $last_slash = mb_strrpos($source,"/");
        $filename = $nid6."_".mb_substr($source,$last_slash, strlen($source));
        $filename = str_replace($ext,"",$filename);
        $filename = Misc::cleanFileName($filename);
        $filename .= $ext;
        $new_path = 'public://'.$dst. $filename;
        $new_image_file = file_save_data($image_file_data, $new_path, FILE_EXISTS_REPLACE);
        $translated_node->set($node_field,["target_id"=>$new_image_file->id()]);
      }
      $source_field = "image_main";
      $dst = "images/downloads/main/";
      $node_field = "field_image";
      if ($translation->$source_field != null){
        $source = $translation->$source_field;
        $image_file_data = file_get_contents('https://us-en.superbook.cbn.com/'.$source);
        $ext = ".png";
        $last_dot = mb_strrpos($source,".");
        if ($last_dot > 0 ){
          $ext = mb_substr($source,$last_dot, strlen($source));
        }
        $last_slash = mb_strrpos($source,"/");
        $filename = $nid6."_".mb_substr($source,$last_slash, strlen($source));
        $filename = str_replace($ext,"",$filename);
        $filename = Misc::cleanFileName($filename);
        $filename .= $ext;
        $new_path = 'public://'.$dst. $filename;
        $new_image_file = file_save_data($image_file_data, $new_path, FILE_EXISTS_REPLACE);
        $translated_node->set($node_field,["target_id"=>$new_image_file->id()]);
      }
      $source_field = "image_small";
      $dst = "images/downloads/thumb/";
      $node_field = "field_thumbnail";
      if ($translation->$source_field != null){
        $source = $translation->$source_field;
        $image_file_data = file_get_contents('https://us-en.superbook.cbn.com/'.$source);
        $ext = ".png";
        $last_dot = mb_strrpos($source,".");
        if ($last_dot > 0 ){
          $ext = mb_substr($source,$last_dot, strlen($source));
        }
        $last_slash = mb_strrpos($source,"/");
        $filename = $nid6."_".mb_substr($source,$last_slash, strlen($source));
        $filename = str_replace($ext,"",$filename);
        $filename = Misc::cleanFileName($filename);
        $filename .= $ext;
        $new_path = 'public://'.$dst. $filename;
        $new_image_file = file_save_data($image_file_data, $new_path, FILE_EXISTS_REPLACE);
        $translated_node->set($node_field,["target_id"=>$new_image_file->id()]);
      }
      if(intval($translation->status) == 1){
        $translated_node->setPublished(true);
      }
      else{
        $translated_node->setPublished(false);
      }
      $translated_node->save();
    }
  }
}
