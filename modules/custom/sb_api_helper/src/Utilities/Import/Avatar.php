<?php
/**
 * @file
 * Contains \Drupal\sb_api_helper\Utilities\Import\Avatar.
 */

namespace Drupal\sb_api_helper\Utilities\Import;
use Drupal\node\Entity\Node;
use Drupal\sb_api_helper\Utilities\Import\Misc;
class Avatar{
  public static function importAvatarItems($index = 0){
    \set_time_limit(36000);
    ini_set('memory_limit','384M');
    $start_time = \microtime(true);
    $existing_nodes = [];
    $result = db_query("select entity_id, field_nid6_value from node__field_nid6");
    $data = $result->fetchAll();
    $nodes_saved = 0;
    foreach($data as $datum){
      $existing_nodes[$datum->field_nid6_value] = $datum->entity_id;
    }
    $import_data = file_get_contents("http://en.superbook.tv/a/admin/avatar_item_import");
    $data = json_decode($import_data);
    $per_page = 30;
    $offset = $index*$per_page;
    $original_count = count($data);
    $data = array_slice($data,$offset,$per_page);
    $available_count = count($data);
    foreach($data as $datum){
      $image_file_data = file_get_contents('http://cdn.superbook.cbn.com/'.$datum->image);
      $ext = ".png";
      echo $datum->image.":\n";
      $last_dot = mb_strrpos($datum->image,".");
      if ($last_dot > 0 ){
        $ext = mb_substr($datum->image,$last_dot, strlen($datum->image));
      }
      $new_path = 'public://avatar_items/images/'.Misc::cleanFileName($datum->category).'/'.Misc::cleanFileName($datum->nid6) . $ext;
      echo "\tsaving to $new_path\n";
      $new_image_file = file_save_data($image_file_data, $new_path, FILE_EXISTS_REPLACE);
      if(!$new_image_file){
        error_log("no new image file, for now do nothing");
      }
      else{
        if (isset($existing_nodes[$datum->nid6])){
          $node = node_load($existing_nodes[$datum->nid6]);
          $node->set('field_avatar_item_category',$datum->category);
          $node->set('field_avatar_item_price',$datum->price);
          $node->set('field_avatar_item_image',[
            'target_id' => $new_image_file->id(),
          ]);
        }
        else{
          /*
          $thumbnail_file_data = file_get_contents('http://cdn.superbook.cbn.com/'.$datum->thumbnail);
          $thumbnail_file = file_save_data($thumbnail_file_data, 'public://avatar_items/thumbnails/'.Misc::cleanFileName($datum->category).'/'.Misc::cleanFileName($datum->title) . '.png', FILE_EXISTS_REPLACE);
          if(!$thumbnail_file){
            die("save failed");
          }
          */


          // Create node object with attached file.
          $node = Node::create([
            'type'        => 'avatar_item',
            'title'       => $datum->title,
            'field_nid6' => $datum->nid6,
            'field_avatar_item_category' => $datum->category,
            'field_avatar_item_price' => $datum->price,
            'field_avatar_item_image' => [
              'target_id' => $new_image_file->id(),
            ],
          ]);
        }
        if ($datum->gender == "Male"){
          $node->set('field_avatar_item_gender', "m");
        }
        if ($datum->gender == "Female"){
          $node->set('field_avatar_item_gender', "f");
        }

        $node->save();
        $nodes_saved++;
      }
      $end_time= \microtime(true) - $start_time;
    }
    return array(
      '#type' => 'markup',
      '#cache'=>['max-age'=>0],
      '#markup' => "Finished $nodes_saved nodes in $end_time seconds. Index: $index. Processed $available_count of $original_count nodes.",
    );
  }
}
