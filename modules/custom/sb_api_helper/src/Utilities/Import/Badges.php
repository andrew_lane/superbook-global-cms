<?php
/**
 * @file
 * Contains \Drupal\sb_api_helper\Utilities\Import\Badges.
 */

namespace Drupal\sb_api_helper\Utilities\Import;
use Drupal\node\Entity\Node;
class Badges{
  public static function linkBadges(){
    //link badges (prerequisites? and master badges)
    \set_time_limit(36000);
    ini_set('memory_limit','256M');

    $result = db_query("select content_field_master_badge_criteria.nid as master_nid6, delta, field_master_badge_criteria_nid as criteria_nid6
     from al_superbook_6.content_field_master_badge_criteria
    inner join al_superbook_6.node on node.vid = content_field_master_badge_criteria.vid
    and node.language = 'en'
    and node.status = 1
    and field_master_badge_criteria_nid IS NOT NULL
    ORDER BY node.vid, delta");
    $data = $result->fetchAll();
    $badges6 = array();
    foreach($data as $datum){
      $badges6[$datum->master_nid6][$datum->delta] = $datum->criteria_nid6;
    }


    $entity_ids = array();
    $result = db_query("select entity_id, field_nid6_value from node__field_nid6
  where bundle = 'badge'");
    $data = $result->fetchAll();
    foreach($data as $datum){
      $entity_ids[$datum->field_nid6_value] = $datum->entity_id;
    }

    $badges8 = array();
    foreach($badges6 as $master_nid_6=>$criteria_array){
      $master_nid_8 = $entity_ids[$master_nid_6];
      $badge = \Drupal\node\Entity\Node::load($master_nid_8);
      $master_criteria = array();
      foreach($criteria_array as $delta=>$criteria_nid_6){
        $criteria_nid_8 = $entity_ids[$criteria_nid_6];
        $badges8[$master_nid_8][$delta] = $criteria_nid_8;
        $master_criteria[$delta] = $criteria_nid_8;
      }
      $badge->set('field_master_badge_criteria', $master_criteria);
      $badge->save();
    }

    return array(
      '#type' => 'markup',
      '#markup' => "Finished.",
    );
  }
  public static function importBadges(){
    \set_time_limit(36000);
    ini_set('memory_limit','256M');

    $existing_nodes = [];
    $result = db_query("select entity_id, field_nid6_value from node__field_nid6");
    $data = $result->fetchAll();
    foreach($data as $datum){
      $existing_nodes[$datum->field_nid6_value] = $datum->entity_id;
    }
    $badges = array();
    $result = db_query("select
    node.tnid,
    node.nid,
    node.title,
    node.language,
    field_trophy_requirement_type_value as requirement_type,
    field_trophy_requirement_value_value as requirement_value,
    (select filepath from al_superbook_6.files where fid = field_badge_image_full_fid) as filepath,
    body,
    nid8.entity_id as game_nid8
    from  al_superbook_6.content_type_trophy
    INNER JOIN al_superbook_6.node ON node.vid = content_type_trophy.vid
    INNER JOIN al_superbook_6.node_revisions ON node.vid = node_revisions.vid
    LEFT JOIN superbookd8_dev_content.node__field_nid6 nid8 ON nid8.field_nid6_value = field_trophy_game_nid
    ");
    $data = $result->fetchAll();
    foreach($data as $datum){
      $tnid = $datum->tnid;
      if (($tnid == 0) || ($tnid == "0")){
        $tnid = $datum->nid;
      }
      if (!isset($badges[$tnid])){
        $badges[$tnid] = new \stdClass();
        $badges[$tnid]->translations = array();
      }
      if ($datum->language == 'en'){
        $badges[$tnid]->requirement_type = $datum->requirement_type;
        $badges[$tnid]->requirement_value = $datum->requirement_value;
        $badges[$tnid]->image_path = $datum->filepath;
        $badges[$tnid]->game_entity_id = $datum->game_nid8;
      }
      $badges[$tnid]->translations[$datum->language] = new \stdClass();
      $badges[$tnid]->translations[$datum->language]->title = $datum->title;
      $badges[$tnid]->translations[$datum->language]->description = $datum->body;
    }

    foreach($badges as $nid6=>$badge){


      if (isset($existing_nodes[$nid6])){
        $node = node_load($existing_nodes[$nid6]);
      }
      else{
        $badge_image_data = file_get_contents('http://cdn.superbook.cbn.com/'.$badge->image_path);
        $badge_file = file_save_data($badge_image_data, 'public://badges/' . $nid6 . '.png', FILE_EXISTS_REPLACE);
        if(!$badge_file){
          die("save failed");
        }
        // Create node object with attached file.
        $node = Node::create([
          'type'        => 'badge',
          'title'       => $badge->translations['en']->title,
          'field_nid6' => $nid6,
          'field_master_content_key' => $nid6,
          'field_badge_description' => $badge->translations['en']->description,
          'field_badge_image' => [
          'target_id' => $badge_file->id(),
          ],
          'field_badge_source' => array($badge->game_entity_id),
          'field_badge_requirement_type'=>$badge->requirement_type,
          'field_badge_requirement_value'=>$badge->requirement_value,
          'langcode' => 'en'
         ]);
         $node->save();
      }


      foreach($badge->translations as $language=>$translation){
        if($language != "en"){
         if($language != "tl"){
           if($language != "it"){
             if($language != "et"){
               if($language != "hy"){
                 if($language != "bn"){
                   if($language != "ta"){
                     if($language != "te"){
                       if($language != "en-ie"){
                         if($language != "en-id"){
                           if($language != "en-ke"){
                             if($language != "pirate"){
                               if (!$node->hasTranslation($language)){
                                 $translated_node = $node->addTranslation($language);
                               }
                               $translated_node = $node->getTranslation($language);
                               $translated_node->set('title',$translation->title);
                               $translated_node->set('field_badge_description', $translation->description);
                               $translated_node->save();
                            }
                           }
                         }
                       }
                     }
                   }
                 }
               }
             }
           }
         }
        }
        else{
          if (!$node->hasTranslation($language)){
            $translated_node = $node->addTranslation($language);
          }
         $translated_node = $node->getTranslation($language);
         $translated_node->set('title',$translation->title);
         $translated_node->set('field_badge_description', $translation->description);
         $translated_node->set('field_master_content_key',$nid6);
         $translated_node->save();
        }
      }
    }
    return array(
      '#type' => 'markup',
      '#markup' => "Finished.",
    );
  }
}
