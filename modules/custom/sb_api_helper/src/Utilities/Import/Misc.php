<?php
/**
 * @file
 * Contains \Drupal\sb_api_helper\Utilities\Import\Misc.
 */

namespace Drupal\sb_api_helper\Utilities\Import;

class Misc{
  public static function downloadImageField($source_input,$source_field,$dst,$node_field,&$node){
    if ($source_input->$source_field != null){
      $source = $source_input->$source_field;
      $image_file_data = file_get_contents('https://us-en.superbook.cbn.com/'.$source);
      $ext = ".png";
      $last_dot = mb_strrpos($source,".");
      if ($last_dot > 0 ){
        $ext = mb_substr($source,$last_dot, strlen($source));
      }
      $last_slash = mb_strrpos($source,"/");
      $filename = $nid6."_".mb_substr($source,$last_slash, strlen($source));
      $filename = str_replace($ext,"",$filename);
      $filename = Misc::cleanFileName($filename);
      $filename .= $ext;
      $new_path = 'public://'.$dst. $filename;
      $new_image_file = file_save_data($image_file_data, $new_path, FILE_EXISTS_REPLACE);
      $node->set($node_field,["target_id"=>$new_image_file->id()]);
    }
  }
  public static function cleanFileName($title){
    $title = str_replace("/"," ",$title);
    $title = str_replace("."," ",$title);
    $title = str_replace("\\"," ",$title);
    $title = str_replace("  "," ",$title);
    $title = str_replace("  "," ",$title);
    $title = str_replace("  "," ",$title);
    $title = str_replace(" ","_",$title);
    return $title;
  }

  public static function getFileExt($path){
    $ext = strrpos($path,".");
    $type = substr($path,$ext,strlen($path));
    return $type;
  }
  public static function downloadImage($folder,$cat,$title,$path){
    $dl_path = "public://images/$folder/$cat/$title".Misc::getFileExt($path);
    $image_file_data = file_get_contents('https://us-en.superbook.cbn.com/'.$path);
    $image_file = file_save_data($image_file_data, $dl_path, FILE_EXISTS_REPLACE);
    if ($image_file == false){
      die("could not save $dl_path");
    }
    return ["target_id"=>$image_file->id()];
  }
  public static function fixBibleReferences(){

    //example: 915872
    $db = \Drupal\Core\Database\Database::getConnection();
    //$result = $db->query($id_query)->execute();
    $result = $db->select("node__field_bible_references","br")
      ->fields("br",["entity_id","field_bible_references_value"])
      ->condition("field_bible_references_value","%-%","LIKE")
      ->execute();
    $records = [];
    foreach($result as $row){
      $records[$row->entity_id][$row->field_bible_references_value] = "";
    }
    $result = $db->select("node__field_bible_references","br")
      ->fields("br",["entity_id","field_bible_references_value"])
      ->condition("field_bible_references_value","%,%","LIKE")
      ->execute();
    foreach($result as $row){
      $records[$row->entity_id][$row->field_bible_references_value] = "";
    }
    $ids_to_fix = array_keys($records);
    foreach($ids_to_fix as $entity_id){
      $node = Node::load($entity_id);
      $bible_references = $node->get('field_bible_references')->getValue();
      $keyed_individual_bible_references = [];
      foreach($bible_references as $bible_reference){
        $bible_reference_key = $bible_reference["value"];
        $ref_parts = explode(".",$bible_reference_key);
        $book_key = $ref_parts[0];
        $chapter_number = $ref_parts[1];
        $verse_range = $ref_parts[2];
        if (strpos($verse_range,",") > 0){
          $range_groups = explode(",",$verse_range);
          foreach($range_groups as $verse_range_sub){
            if (strpos($verse_range_sub,"-") > 0){
              $range_parts = explode("-",$verse_range_sub);
              $range_min = $range_parts[0];
              $range_max = $range_parts[1];
              for($i = $range_min; $i <= $range_max; $i++){
                $new_reference = "$book_key.$chapter_number.$i";
                $keyed_individual_bible_references[$new_reference] = $new_reference;
              }
            }
            else{
              $new_reference = "$book_key.$chapter_number.$verse_range_sub";
              $keyed_individual_bible_references[$new_reference] = $new_reference;
            }
          }
        }
        elseif (strpos($verse_range,"-") > 0){
          $range_parts = explode("-",$verse_range);
          $range_min = $range_parts[0];
          $range_max = $range_parts[1];
          for($i = $range_min; $i <= $range_max; $i++){
            $new_reference = "$book_key.$chapter_number.$i";
            $keyed_individual_bible_references[$new_reference] = $new_reference;
          }
        }
        else{
          $keyed_individual_bible_references[$bible_reference_key] = $bible_reference_key;
        }
      }
      $bible_references_to_save = array_keys($keyed_individual_bible_references);

      $node->set('field_bible_references',$bible_references_to_save);
      $node->save();
    }
    $query = $db->select("node__field_bible_references","br")
      ->fields("br",["entity_id","revision_id","langcode","field_bible_references_value"]);
    $query->addExpression('count(field_bible_references_value)', 'duplicate_count');
    $query->addExpression('min(delta)', 'min_delta');
    $query->groupBy('entity_id, revision_id, langcode, field_bible_references_value');
    $query->having('count(field_bible_references_value) > :matches', [':matches' => 1]);
    $result = $query->execute();
    $records = [];
    foreach($result as $row){
      $query2 = $db->delete("node__field_bible_references")
      ->condition("revision_id",$row->revision_id)
      ->condition("langcode",$row->langcode)
      ->condition("field_bible_references_value",$row->field_bible_references_value)
      ->condition("delta",$row->min_delta,">");
      $result2 = $query2->execute();

    }
    echo "done";
    exit;
  }
  public static function bibleBookFromNid6($nid6){
    switch(intval($nid6)){
      case 312781:
      case 673166:
      case 709021:
      case 724776:
      case 731216:
      case 732276:
      case 732906:
      case 734326:
      case 839631:
      case 853271:
      case 1205141:
      case 1252191:
      case 1445711:
      case 1468281:
      case 1794796:
      case 1832946:
      case 2078631:
      case 2092106:
      case 2191521:
      case 2204316:
      case 2558406:
      case 2640876:
      case 2777076:
        return "GEN";//1
        break;
      case 312786:
      case 673171:
      case 709261:
      case 724906:
      case 731276:
      case 732336:
      case 732966:
      case 734431:
      case 839481:
      case 860076:
      case 1204716:
      case 1259001:
      case 1446201:
      case 1468781:
      case 1794591:
      case 1832741:
      case 2079121:
      case 2092396:
      case 2193386:
      case 2204616:
      case 2558891:
      case 2641366:
        return "EXO";//2
        break;
      case 312791:
      case 673176:
      case 709256:
      case 731271:
      case 731601:
      case 732331:
      case 732961:
      case 734426:
      case 839976:
      case 860071:
      case 1204706:
      case 1258996:
      case 1446196:
      case 1468776:
      case 1794701:
      case 1832851:
      case 2079116:
      case 2092391:
      case 2193456:
      case 2204611:
      case 2558886:
      case 2641361:
        return "LEV";//3
        break;
      case 312796:
      case 673181:
      case 709251:
      case 731351:
      case 731656:
      case 732411:
      case 733041:
      case 734421:
      case 839716:
      case 860066:
      case 1204711:
      case 1258991:
      case 1446191:
      case 1468771:
      case 1794586:
      case 1832736:
      case 2079111:
      case 2092386:
      case 2193261:
      case 2204606:
      case 2558881:
      case 2641356:
        return "NUM";//4
        break;
      case 312801:
      case 673186:
      case 709246:
      case 731346:
      case 731651:
      case 732406:
      case 733036:
      case 734481:
      case 839906:
      case 860061:
      case 1204701:
      case 1258986:
      case 1446186:
      case 1468766:
      case 1794581:
      case 1832731:
      case 2079106:
      case 2092381:
      case 2193256:
      case 2204601:
      case 2558876:
      case 2641351:
        return "DEU";//5
        break;
      case 312806:
      case 673191:
      case 709241:
      case 731341:
      case 731646:
      case 732401:
      case 733031:
      case 734416:
      case 839901:
      case 853266:
      case 1204696:
      case 1252186:
      case 1446181:
      case 1468761:
      case 1794576:
      case 1832726:
      case 2079101:
      case 2092376:
      case 2191516:
      case 2204596:
      case 2558871:
      case 2641346:
        return "JOS";//6
        break;
      case 312811:
      case 673196:
      case 710011:
      case 731966:
      case 733496:
      case 839896:
      case 853261:
      case 944911:
      case 1000021:
      case 1201441:
      case 1204691:
      case 1252181:
      case 1446176:
      case 1468756:
      case 1794571:
      case 1832721:
      case 2079096:
      case 2092371:
      case 2191511:
      case 2204591:
      case 2558866:
      case 2641341:
        return "JDG";//7
        break;
      case 312816:
      case 673201:
      case 711121:
      case 853256:
      case 1003466:
      case 1252176:
      case 1446171:
      case 1468751:
      case 1499081:
      case 1510151:
      case 2079091:
      case 2191506:
      case 2204096:
      case 2205711:
      case 2496796:
      case 2558861:
      case 2641336:
      case 2750531:
      case 3871001:
        return "RUT";//8
        break;
      case 312821:
      case 673206:
      case 710181:
      case 724661:
      case 815301:
      case 839711:
      case 853251:
      case 945091:
      case 1000061:
      case 1201571:
      case 1204686:
      case 1252171:
      case 1446166:
      case 1468746:
      case 1498746:
      case 1794791:
      case 1832941:
      case 2079086:
      case 2191501:
      case 2203846:
      case 2558856:
      case 2641331:
        return "1SA";//9
        break;
      case 312826:
      case 673211:
      case 710991:
      case 734411:
      case 815391:
      case 838451:
      case 840136:
      case 853246:
      case 1000016:
      case 1201901:
      case 1204681:
      case 1252166:
      case 1446161:
      case 1468741:
      case 1498846:
      case 1794921:
      case 1833066:
      case 2079081:
      case 2191496:
      case 2203911:
      case 2558851:
      case 2641326:
        return "2SA";//10
        break;
      case 312831:
      case 673216:
      case 709236:
      case 734406:
      case 815141:
      case 838241:
      case 839891:
      case 853411:
      case 1000011:
      case 1201436:
      case 1204676:
      case 1252331:
      case 1446156:
      case 1468736:
      case 1498766:
      case 1794566:
      case 1832716:
      case 2079076:
      case 2192016:
      case 2203861:
      case 2558846:
      case 2641321:
        return "1KI";//11
        break;
      case 312841:
      case 673221:
      case 709231:
      case 731926:
      case 733426:
      case 734401:
      case 839061:
      case 853636:
      case 1000006:
      case 1201696:
      case 1204286:
      case 1252556:
      case 1446151:
      case 1468731:
      case 1499076:
      case 1794916:
      case 1833061:
      case 2079071:
      case 2192011:
      case 2204091:
      case 2558841:
      case 2641316:
        return "2KI";//12
        break;
      case 312846:
      case 673226:
      case 709226:
      case 731336:
      case 731641:
      case 732396:
      case 733026:
      case 734476:
      case 839056:
      case 853241:
      case 1204281:
      case 1252161:
      case 1446146:
      case 1468726:
      case 1498841:
      case 1794911:
      case 1833056:
      case 2079066:
      case 2191491:
      case 2203906:
      case 2558836:
      case 2641311:
        return "1CH";//13
        break;
      case 312851:
      case 673231:
      case 709016:
      case 815006:
      case 837566:
      case 839051:
      case 853236:
      case 944906:
      case 1000001:
      case 1201431:
      case 1204276:
      case 1252156:
      case 1446141:
      case 1468721:
      case 1499071:
      case 1794561:
      case 1832711:
      case 2079061:
      case 2191486:
      case 2204086:
      case 2558831:
      case 2641306:
        return "2CH";//14
        break;
      case 312856:
      case 673236:
      case 710176:
      case 815001:
      case 837561:
      case 839046:
      case 853221:
      case 944981:
      case 999996:
      case 1201566:
      case 1204271:
      case 1252141:
      case 1446136:
      case 1468716:
      case 1499066:
      case 1794666:
      case 1832816:
      case 2079056:
      case 2191471:
      case 2204081:
      case 2558826:
      case 2575041:
      case 2641301:
        return "EZR";//15
        break;
      case 312861:
      case 673241:
      case 709221:
      case 814996:
      case 837556:
      case 839041:
      case 853216:
      case 944901:
      case 999991:
      case 1201426:
      case 1204266:
      case 1252136:
      case 1446131:
      case 1468711:
      case 1499061:
      case 1794556:
      case 1832706:
      case 2079051:
      case 2191466:
      case 2204076:
      case 2558821:
      case 2575036:
      case 2641296:
       return "NEH";//16
        break;
      case 312866:
      case 673246:
      case 710496:
      case 731851:
      case 733331:
      case 860056:
      case 1004191:
      case 1258981:
      case 1446126:
      case 1468706:
      case 1721611:
      case 1795516:
      case 2079046:
      case 2092366:
      case 2095641:
      case 2192006:
      case 2200221:
      case 2204586:
      case 2558816:
      case 2574921:
      case 2641291:
      case 3870996:
        return "EST";//17
        break;
      case 312871:
      case 673251:
      case 712661:
      case 838061:
      case 839626:
      case 860051:
      case 946161:
      case 1000056:
      case 1205136:
      case 1215716:
      case 1258976:
      case 1446121:
      case 1468701:
      case 1516491:
      case 1796131:
      case 1833696:
      case 2079041:
      case 2092361:
      case 2193296:
      case 2204581:
      case 2558811:
      case 2641286:
        return "JOB";//18
        break;
      case 312876:
      case 673256:
      case 709216:
      case 731331:
      case 731636:
      case 732391:
      case 733021:
      case 734471:
      case 839886:
      case 853461:
      case 1204671:
      case 1252381:
      case 1446116:
      case 1468696:
      case 1498836:
      case 1794551:
      case 1832701:
      case 2079036:
      case 2192001:
      case 2203901:
      case 2558806:
      case 2641281:
        return "PSA";//19
        break;
      case 312881:
      case 673261:
      case 711116:
      case 853631:
      case 1003996:
      case 1252551:
      case 1446111:
      case 1468691:
      case 1499056:
      case 1510256:
      case 2079031:
      case 2195046:
      case 2204071:
      case 2205706:
      case 2497986:
      case 2558801:
      case 2641276:
      case 2750951:
      case 3870991:
        return "PRO";//20
        break;
      case 312886:
      case 673266:
      case 711111:
      case 853626:
      case 1003991:
      case 1252546:
      case 1446106:
      case 1468686:
      case 1499051:
      case 1510251:
      case 2079026:
      case 2095636:
      case 2191996:
      case 2200216:
      case 2204066:
      case 2558796:
      case 2639921:
      case 2641271:
      case 3870986:
        return "ECC";//21
        break;
      case 312891:
      case 673271:
      case 853621:
      case 1003726:
      case 1252541:
      case 1446101:
      case 1468681:
      case 1499046:
      case 1510246:
      case 1809251:
      case 2079021:
      case 2095631:
      case 2191991:
      case 2200211:
      case 2204061:
      case 2558791:
      case 2639916:
      case 2641266:
      case 3870981:
        return "SOS";//22
        break;
      case 312896:
      case 673276:
      case 709211:
      case 725531:
      case 733351:
      case 839101:
      case 853456:
      case 944896:
      case 1000521:
      case 1201691:
      case 1204206:
      case 1252376:
      case 1446096:
      case 1468676:
      case 1498831:
      case 1794546:
      case 1832696:
      case 2079016:
      case 2191986:
      case 2203896:
      case 2558786:
      case 2574956:
      case 2641261:
      case 2777051:
        return "ISA";//23
        break;
      case 312901:
      case 673281:
      case 709206:
      case 731856:
      case 733346:
      case 839036:
      case 853231:
      case 944891:
      case 999986:
      case 1201461:
      case 1204261:
      case 1252151:
      case 1446091:
      case 1468671:
      case 1499041:
      case 1794541:
      case 1832691:
      case 2079011:
      case 2191481:
      case 2204056:
      case 2558781:
      case 2574951:
      case 2641256:
        return "JER";//24
        break;
      case 312906:
      case 673286:
      case 714866:
      case 815386:
      case 838446:
      case 840131:
      case 860046:
      case 945186:
      case 1001596:
      case 1205916:
      case 1215251:
      case 1258971:
      case 1446086:
      case 1468666:
      case 1794906:
      case 1833051:
      case 2079006:
      case 2092356:
      case 2191981:
      case 2204576:
      case 2558776:
      case 2641251:
        return "LAM";//25
        break;
      case 312911:
      case 673291:
      case 709201:
      case 731326:
      case 731631:
      case 732386:
      case 733016:
      case 734466:
      case 839031:
      case 853616:
      case 1204256:
      case 1252536:
      case 1446081:
      case 1468661:
      case 1499036:
      case 1794901:
      case 1833046:
      case 2079001:
      case 2191976:
      case 2204051:
      case 2558771:
      case 2575001:
      case 2641246:
      case 2777256:
        return "EZE";//26
        break;
      case 312916:
      case 673296:
      case 710266:
      case 724721:
      case 733336:
      case 838946:
      case 852676:
      case 945181:
      case 1000926:
      case 1201686:
      case 1204131:
      case 1251596:
      case 1446076:
      case 1468656:
      case 1794896:
      case 1833041:
      case 2078996:
      case 2092351:
      case 2191626:
      case 2204571:
      case 2558766:
      case 2574916:
      case 2641241:
      case 2777236:
        return "DAN";//27
        break;
      case 312921:
      case 673301:
      case 711101:
      case 815296:
      case 838376:
      case 840046:
      case 853611:
      case 945086:
      case 1001516:
      case 1201896:
      case 1205311:
      case 1252531:
      case 1446071:
      case 1468651:
      case 1499031:
      case 1794786:
      case 1832936:
      case 2078991:
      case 2193521:
      case 2204046:
      case 2558761:
      case 2641236:
        return "HOS";//28
        break;
      case 312926:
      case 673306:
      case 714861:
      case 815381:
      case 838441:
      case 840126:
      case 860041:
      case 945176:
      case 1001591:
      case 1205131:
      case 1215246:
      case 1258966:
      case 1446066:
      case 1468646:
      case 1794891:
      case 1833036:
      case 2078986:
      case 2092346:
      case 2191971:
      case 2204566:
      case 2558756:
      case 2641231:
        return "JOE";//29
        break;
      case 312931:
      case 673311:
      case 711096:
      case 815291:
      case 838371:
      case 840041:
      case 853606:
      case 945081:
      case 1001511:
      case 1205846:
      case 1215191:
      case 1252526:
      case 1446061:
      case 1468641:
      case 1499026:
      case 1794781:
      case 1832931:
      case 2078981:
      case 2191966:
      case 2204041:
      case 2558751:
      case 2641226:
        return "AMO";//30
        break;
      case 312936:
      case 673316:
      case 714856:
      case 731321:
      case 731626:
      case 732381:
      case 733011:
      case 734461:
      case 840856:
      case 860036:
      case 1205371:
      case 1258961:
      case 1446056:
      case 1468636:
      case 2078976:
      case 2092341:
      case 2191961:
      case 2200206:
      case 2204561:
      case 2558746:
      case 2641221:
        return "OBA";//31
        break;
      case 312941:
      case 673321:
      case 709611:
      case 724171:
      case 733341:
      case 860031:
      case 1004386:
      case 1258956:
      case 1446051:
      case 1468631:
      case 2078971:
      case 2092336:
      case 2195041:
      case 2204556:
      case 2205701:
      case 2497981:
      case 2558741:
      case 2641216:
      case 3870976:
        return "JON";//32
        break;
      case 312946:
      case 673326:
      case 709196:
      case 815376:
      case 838436:
      case 840121:
      case 853226:
      case 945171:
      case 1001586:
      case 1201561:
      case 1205316:
      case 1252146:
      case 1446046:
      case 1468626:
      case 1794886:
      case 1833031:
      case 2078966:
      case 2092331:
      case 2191476:
      case 2204551:
      case 2558736:
      case 2641211:
        return "MIC";//33
        break;
      case 312951:
      case 673331:
      case 709671:
      case 731921:
      case 733421:
      case 840116:
      case 860026:
      case 945166:
      case 1001581:
      case 1205911:
      case 1215241:
      case 1258951:
      case 1446041:
      case 1468621:
      case 1794881:
      case 1833026:
      case 2078961:
      case 2092326:
      case 2193581:
      case 2204546:
      case 2558731:
      case 2641206:
        return "NAH";//34
        break;
      case 312956:
      case 673336:
      case 715146:
      case 860021:
      case 1005031:
      case 1258946:
      case 1446036:
      case 1468616:
      case 1516486:
      case 2078956:
      case 2092321:
      case 2195036:
      case 2204541:
      case 2205696:
      case 2497976:
      case 2558726:
      case 2641201:
      case 2750946:
      case 3870971:
        return "HAB";//35
        break;
      case 312961:
      case 673341:
      case 709666:
      case 731916:
      case 733416:
      case 840731:
      case 860016:
      case 1004416:
      case 1205231:
      case 1258941:
      case 1446031:
      case 1468611:
      case 1735241:
      case 2078951:
      case 2092316:
      case 2191956:
      case 2200201:
      case 2204536:
      case 2558721:
      case 2641196:
        return "ZEP";//36
        break;
      case 312966:
      case 673346:
      case 710661:
      case 815371:
      case 838431:
      case 840111:
      case 860011:
      case 945161:
      case 1001576:
      case 1205906:
      case 1215236:
      case 1258936:
      case 1446026:
      case 1468606:
      case 1794876:
      case 1833021:
      case 2078946:
      case 2092311:
      case 2193576:
      case 2204531:
      case 2558716:
      case 2575031:
      case 2641191:
        return "HAG";//37
        break;
      case 312971:
      case 673351:
      case 710656:
      case 815366:
      case 838056:
      case 839621:
      case 853601:
      case 945156:
      case 1000516:
      case 1205126:
      case 1215231:
      case 1252521:
      case 1446021:
      case 1468601:
      case 1499021:
      case 1794871:
      case 1833016:
      case 2078941:
      case 2191951:
      case 2204036:
      case 2558711:
      case 2575026:
      case 2641186:
        return "ZEC";//38
        break;
      case 312976:
      case 673356:
      case 710006:
      case 815626:
      case 840376:
      case 860006:
      case 945421:
      case 1001811:
      case 1201421:
      case 1205306:
      case 1258931:
      case 1446016:
      case 1468596:
      case 1718361:
      case 1795196:
      case 1833306:
      case 2078936:
      case 2092306:
      case 2191946:
      case 2204526:
      case 2558706:
      case 2641181:
        return "MAL";//39
        break;
      case 312981:
      case 673361:
      case 709191:
      case 724311:
      case 731316:
      case 732376:
      case 733006:
      case 734456:
      case 839616:
      case 852101:
      case 1204841:
      case 1251021:
      case 1446011:
      case 1468591:
      case 1499016:
      case 1794866:
      case 1833011:
      case 2078931:
      case 2191461:
      case 2204031:
      case 2558701:
      case 2574996:
      case 2641176:
        return "MAT";//40
        break;
      case 312986:
      case 673366:
      case 709186:
      case 724306:
      case 815196:
      case 839611:
      case 852631:
      case 944976:
      case 999981:
      case 1201681:
      case 1204836:
      case 1251551:
      case 1446006:
      case 1468586:
      case 1499011:
      case 1794661:
      case 1832811:
      case 2078926:
      case 2191556:
      case 2204026:
      case 2558696:
      case 2641171:
        return "MAR";//41
        break;
      case 312991:
      case 673371:
      case 709181:
      case 724301:
      case 733326:
      case 839456:
      case 852096:
      case 944971:
      case 999976:
      case 1201556:
      case 1204831:
      case 1251016:
      case 1446001:
      case 1468581:
      case 1499006:
      case 1794656:
      case 1832806:
      case 2078921:
      case 2191456:
      case 2204021:
      case 2558691:
      case 2641166:
        return "LUK";//42
        break;
      case 312996:
      case 673376:
      case 709176:
      case 724401:
      case 731366:
      case 732426:
      case 733056:
      case 734496:
      case 839606:
      case 852811:
      case 1205301:
      case 1251731:
      case 1445996:
      case 1468576:
      case 1499001:
      case 1794696:
      case 1832846:
      case 2078916:
      case 2191451:
      case 2204016:
      case 2558686:
      case 2641161:
        return "JOH";//43
        break;
      case 313001:
      case 673381:
      case 709171:
      case 724831:
      case 815136:
      case 839451:
      case 852626:
      case 944886:
      case 1000096:
      case 1201416:
      case 1204966:
      case 1251546:
      case 1445991:
      case 1468571:
      case 1498826:
      case 1794536:
      case 1832686:
      case 2078911:
      case 2191551:
      case 2203891:
      case 2558681:
      case 2641156:
        return "ACT";//44
        break;
      case 313006:
      case 673386:
      case 709166:
      case 815621:
      case 838051:
      case 839601:
      case 853596:
      case 945416:
      case 1000511:
      case 1201676:
      case 1205261:
      case 1252516:
      case 1445986:
      case 1468566:
      case 1498996:
      case 1795191:
      case 1833301:
      case 2078906:
      case 2191941:
      case 2204011:
      case 2558676:
      case 2641151:
        return "ROM";//45
        break;
      case 313011:
      case 673391:
      case 710261:
      case 725091:
      case 815191:
      case 839596:
      case 860001:
      case 944966:
      case 1000506:
      case 1201671:
      case 1205756:
      case 1258926:
      case 1445981:
      case 1468556:
      case 1794651:
      case 1832801:
      case 2078901:
      case 2092301:
      case 2191826:
      case 2204521:
      case 2558671:
      case 2641146:
        return "1CO";//46
        break;
      case 313016:
      case 673396:
      case 709161:
      case 815616:
      case 838046:
      case 839591:
      case 859996:
      case 945411:
      case 1000501:
      case 1201476:
      case 1258921:
      case 1445976:
      case 1468551:
      case 1792266:
      case 1795186:
      case 1833296:
      case 2078896:
      case 2092296:
      case 2191936:
      case 2204516:
      case 2558666:
      case 2641141:
        return "2CO";//47
        break;
      case 313021:
      case 673401:
      case 709156:
      case 815611:
      case 840371:
      case 859991:
      case 945406:
      case 1001806:
      case 1201411:
      case 1258916:
      case 1445971:
      case 1468541:
      case 1718356:
      case 1792261:
      case 1795181:
      case 1833291:
      case 2078891:
      case 2092291:
      case 2191931:
      case 2204511:
      case 2558661:
      case 2641136:
        return "GAL";//48
        break;
      case 313026:
      case 673406:
      case 712541:
      case 838041:
      case 839586:
      case 859986:
      case 946156:
      case 1000496:
      case 1215711:
      case 1258911:
      case 1445966:
      case 1468536:
      case 1516481:
      case 1792721:
      case 1796126:
      case 1833691:
      case 2078886:
      case 2092286:
      case 2193031:
      case 2204506:
      case 2558656:
      case 2641131:
        return "EPH";//49
        break;
      case 313031:
      case 673411:
      case 712536:
      case 838091:
      case 839706:
      case 859981:
      case 946191:
      case 1000596:
      case 1215771:
      case 1258906:
      case 1445961:
      case 1468531:
      case 1516476:
      case 1792776:
      case 1809396:
      case 1833751:
      case 2078881:
      case 2092281:
      case 2193026:
      case 2204501:
      case 2558651:
      case 2641126:
        return "PHP";//50
        break;
      case 313036:
      case 673416:
      case 712531:
      case 859976:
      case 1003811:
      case 1258901:
      case 1445956:
      case 1468526:
      case 1516471:
      case 1809391:
      case 2078876:
      case 2080701:
      case 2092276:
      case 2095031:
      case 2193021:
      case 2199731:
      case 2204496:
      case 2558646:
      case 2600911:
      case 2641121:
      case 3870966:
        return "COL";//51
        break;
      case 313051:
      case 673421:
      case 712526:
      case 838036:
      case 839581:
      case 859971:
      case 946151:
      case 1000491:
      case 1215706:
      case 1258896:
      case 1445951:
      case 1468521:
      case 1516466:
      case 1792716:
      case 1796121:
      case 1833686:
      case 2078871:
      case 2092271:
      case 2193016:
      case 2204491:
      case 2558641:
      case 2641116:
        return "1TH";//52
        break;
      case 313056:
      case 673426:
      case 712521:
      case 838031:
      case 839576:
      case 859966:
      case 946146:
      case 1000486:
      case 1215701:
      case 1258891:
      case 1445946:
      case 1468516:
      case 1516461:
      case 1792711:
      case 1796116:
      case 1833681:
      case 2078866:
      case 2092266:
      case 2193011:
      case 2204486:
      case 2558636:
      case 2641111:
        return "2TH";//53
        break;
      case 313061:
      case 673431:
      case 710256:
      case 815606:
      case 838026:
      case 839571:
      case 859961:
      case 945401:
      case 1000481:
      case 1201666:
      case 1258886:
      case 1445941:
      case 1468511:
      case 1792256:
      case 1795176:
      case 1833286:
      case 2078861:
      case 2092261:
      case 2193006:
      case 2204481:
      case 2558631:
      case 2641106:
        return "1TI";//54
        break;
      case 313066:
      case 673436:
      case 710251:
      case 815601:
      case 838021:
      case 839566:
      case 853591:
      case 945396:
      case 1000476:
      case 1201661:
      case 1252511:
      case 1445936:
      case 1468506:
      case 1498991:
      case 1792251:
      case 1795171:
      case 1833281:
      case 2078856:
      case 2193001:
      case 2204006:
      case 2558626:
      case 2641101:
        return "2TI";//55
        break;
      case 313071:
      case 673441:
      case 712516:
      case 859956:
      case 1003806:
      case 1258881:
      case 1445931:
      case 1468501:
      case 1516456:
      case 1809386:
      case 2078851:
      case 2080696:
      case 2092256:
      case 2095026:
      case 2192996:
      case 2199726:
      case 2204476:
      case 2558621:
      case 2601861:
      case 2641096:
      case 3870961:
        return "TIT";//56
        break;
      case 313076:
      case 673446:
      case 712511:
      case 859951:
      case 1003801:
      case 1258876:
      case 1445926:
      case 1468496:
      case 1516451:
      case 1809381:
      case 2078846:
      case 2080691:
      case 2092251:
      case 2095021:
      case 2192991:
      case 2199721:
      case 2204471:
      case 2558616:
      case 2601856:
      case 2641091:
      case 3870956:
        return "PHM";//57
        break;
      case 313081:
      case 673451:
      case 709151:
      case 815131:
      case 838016:
      case 839561:
      case 853586:
      case 944881:
      case 999971:
      case 1201406:
      case 1204666:
      case 1252506:
      case 1445921:
      case 1468491:
      case 1498986:
      case 1794531:
      case 1832681:
      case 2078841:
      case 2191926:
      case 2204001:
      case 2558611:
      case 2641086:
        return "HEB";//58
        break;
      case 313086:
      case 673456:
      case 709146:
      case 838011:
      case 839556:
      case 859946:
      case 946141:
      case 1000036:
      case 1201866:
      case 1203486:
      case 1204861:
      case 1258871:
      case 1445916:
      case 1468486:
      case 1796111:
      case 1833676:
      case 2078836:
      case 2092246:
      case 2192986:
      case 2204466:
      case 2558606:
      case 2641081:
        return "JAM";//59
        break;
      case 313091:
      case 673461:
      case 709141:
      case 838006:
      case 839551:
      case 859941:
      case 946136:
      case 1000471:
      case 1215696:
      case 1258866:
      case 1445911:
      case 1468481:
      case 1516446:
      case 1792706:
      case 1796106:
      case 1833671:
      case 2078831:
      case 2092241:
      case 2191921:
      case 2204461:
      case 2558601:
      case 2641076:
        return "1PE";//60
        break;
      case 313096:
      case 673466:
      case 712506:
      case 838001:
      case 839546:
      case 859936:
      case 946131:
      case 1000466:
      case 1215691:
      case 1258861:
      case 1445906:
      case 1468476:
      case 1516441:
      case 1792701:
      case 1796101:
      case 1833666:
      case 2078826:
      case 2092236:
      case 2192981:
      case 2204456:
      case 2558596:
      case 2641071:
        return "2PE";//61
        break;
      case 313101:
      case 673471:
      case 712501:
      case 837996:
      case 839541:
      case 859931:
      case 946126:
      case 1000461:
      case 1215686:
      case 1258856:
      case 1445901:
      case 1468471:
      case 1516436:
      case 1792696:
      case 1796096:
      case 1833661:
      case 2078821:
      case 2092231:
      case 2192976:
      case 2204451:
      case 2558591:
      case 2641066:
        return "1JO";//62
        break;
      case 313106:
      case 673476:
      case 712496:
      case 859926:
      case 1005026:
      case 1258851:
      case 1445896:
      case 1468466:
      case 1516431:
      case 1811121:
      case 2078816:
      case 2080686:
      case 2092226:
      case 2095016:
      case 2192971:
      case 2199716:
      case 2204446:
      case 2558586:
      case 2601851:
      case 2641061:
      case 3870951:
        return "2JO";//63
        break;
      case 313111:
      case 673481:
      case 714286:
      case 859921:
      case 1005021:
      case 1258846:
      case 1445891:
      case 1468461:
      case 1516426:
      case 2078811:
      case 2092221:
      case 2095676:
      case 2193066:
      case 2199851:
      case 2204441:
      case 2558581:
      case 2601871:
      case 2641056:
      case 3870946:
        return "3JO";//64
        break;
      case 313116:
      case 673486:
      case 710246:
      case 815361:
      case 837991:
      case 839536:
      case 859916:
      case 945151:
      case 1000456:
      case 1201656:
      case 1205121:
      case 1258841:
      case 1445886:
      case 1468456:
      case 1794861:
      case 1833006:
      case 2078806:
      case 2092216:
      case 2192966:
      case 2204436:
      case 2558576:
      case 2641051:
      case 2777231:
        return "JDE";//65
        break;
      case 313121:
      case 673491:
      case 710106:
      case 725526:
      case 815286:
      case 839411:
      case 853581:
      case 945076:
      case 1000306:
      case 1201501:
      case 1204661:
      case 1252501:
      case 1445706:
      case 1468276:
      case 1498981:
      case 1794776:
      case 1832926:
      case 2078626:
      case 2191916:
      case 2203996:
      case 2558401:
      case 2640871:
      case 2777056:
        return "REV";//66
        break;
    }
    return false;
  }
}
