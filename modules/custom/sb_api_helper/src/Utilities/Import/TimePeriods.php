<?php
/**
 * @file
 * Contains \Drupal\sb_api_helper\Utilities\Import\TimePeriods.
 */

namespace Drupal\sb_api_helper\Utilities\Import;
use Drupal\node\Entity\Node;
class TimePeriods{
  public static function importTimePeriods(){
    $import_data = file_get_contents("https://us-en.superbook.cbn.com/a/admin/export_time_periods");
    $time_periods = json_decode($import_data);
    $db = \Drupal\Core\Database\Database::getConnection();
    $existing_nodes = [];
    $result = $db->select("node__field_nid6","n6")
      ->fields("n6",["entity_id","field_nid6_value"])
      ->execute();
    foreach($result as $row){
      $existing_nodes[$row->field_nid6_value] = $row->entity_id;
    }
    $count = 0;
    foreach($time_periods as $nid6=>$time_period){
      $node = null;
      if (isset($existing_nodes[$nid6])){
        $node = Node::load($existing_nodes[$nid6]);
        $node->set("title",(string)$time_period->title);
      }
      else{
        $node = Node::create([
          "title"=>(string)$time_period->title,
          "field_nid6"=>$nid6,
          "type"=>"bible_time_period"
        ]);

      }
      $node->set("field_time_number",intval($time_period->number));
      $node->set("field_master_content_key",intval($nid6));
      if(intval($time_period->status) == 1){
        $node->setPublished(true);
      }
      else{
        $node->setPublished(false);
      }
      $node->save();
      $count++;
    }
    echo "$count nodes saved";
    exit;
  }
}
