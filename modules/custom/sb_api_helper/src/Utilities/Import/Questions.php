<?php
/**
 * @file
 * Contains \Drupal\sb_api_helper\Utilities\Import\Questions.
 */

namespace Drupal\sb_api_helper\Utilities\Import;
use Drupal\node\Entity\Node;
class Questions{
  public static function importQuestions(){
    $import_data = file_get_contents("https://us-en.superbook.cbn.com/a/admin/qa_export");
    $questions = json_decode($import_data);
    $batch_operations = [];
    foreach($questions as $nid6=>$question){
      $batch_operations[] = array('\Drupal\sb_api_helper\Utilities\Import\Questions::batchImportQuestion', ["nid6"=>$nid6,"qa"=>$question]);
    }
    $batch = [
			'title' => "Importing Questions and Answers",
			'operations' => $batch_operations,
			//'finished' => '\Drupal\sb_content\Controller\ContentController::finishBookSync',
			//'file' => 'path_to_file_containing_myfunctions',
		];
		batch_set($batch);
  		// Only needed if not inside a form _submit handler.
  		// Setting redirect in batch_process.
		return batch_process('/admin/content');
  }
  public static function batchImportQuestion($nid6,$qa){
    $db = \Drupal\Core\Database\Database::getConnection();
    $existing_nodes = [];
    $result = $db->select("node__field_nid6","n6")
      ->fields("n6",["entity_id","field_nid6_value"])
      ->execute();
    foreach($result as $row){
      $existing_nodes[$row->field_nid6_value] = $row->entity_id;
    }
    $node = null;
    if (!isset($qa->translations->en->title)){
      return;
    }
    if ($qa->translations->en->title == ""){
      return;
    }
    else{
      if (isset($existing_nodes[$nid6])){
        $node_has_changes = false;

        $node = node_load($existing_nodes[$nid6]);
        if (!$node->hasTranslation("en")){
          $node = $node->addTranslation("en");
        }
        $node->set('title', $qa->translations->en->title);
        if ($qa->translations->en->short_title != null){
          $node->set('field_short_title', $qa->translations->en->short_title);
        }
        $node->set('field_qa_answer',$qa->translations->en->body);
        $node->set('field_bible_references',$qa->bible_reference);
        if (isset($qa->tags)){
          if (count($qa->tags) > 0){
            $topic_array = [];
            foreach($qa->tags as $tag_id){
              if (isset($existing_nodes[$tag_id])){
                $topic_array[] = $existing_nodes[$tag_id];
              }
            }
            $node->set('field_bible_topics',$topic_array);
          }
        }
        $nodes_saved++;
        $node->save();
        $valid_node = true;
      }
      else{
        $node = Node::create([
          'type'        => 'qa',
          'title'       => $qa->translations->en->title,
          'field_qa_answer' => $qa->translations->en->body,
          'field_nid6' => $nid6,
          'field_master_content_key'=>$nid6,
          'langcode' => 'en'
        ]);
        $nodes_saved++;
        $node->save();
        $valid_node = true;

      }
      if($valid_node){
        foreach($topic->translations as $language=>$translation){
          $translation_changed = false;
          if (isset($translation->title) && $translation->title != null && $translation->title != ""){
            if($language != "en"){
              if($language != "tl"){
                if($language != "it"){
                  if($language != "et"){
                    if($language != "hy"){
                      if($language != "bn"){
                        if($language != "ta"){
                          if($language != "te"){
                            if($language != "en-ie"){
                              if($language != "en-id"){
                                if($language != "en-ke"){
                                  if($language != "zxx"){
                                    if (!$node->hasTranslation($language)){
                                      $translated_node = $node->addTranslation($language);
                                      $translation_changed = true;
                                    }
                                    $translated_node = $node->getTranslation($language);
                                    $translated_node->set('title', $translation->title);
                                    if ($translation->short_title != null){
                                      $translated_node->set('field_short_title', $translation->short_title);
                                    }
                                    $node->set('field_qa_answer',$translation->body);
                                    //$translated_node->set('field_trivia_correct_answers',$translation->correct_answers);
                                    //$translated_node->set('field_trivia_incorrect_answers',$translation->incorrect_answers);

                                    $nodes_saved++;
                                    $translated_node->save();
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
