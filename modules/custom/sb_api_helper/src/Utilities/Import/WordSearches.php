<?php
/**
 * @file
 * Contains \Drupal\sb_api_helper\Utilities\Import\WordSearches.
 */

namespace Drupal\sb_api_helper\Utilities\Import;
use Drupal\node\Entity\Node;
class WordSearches{
  public static function importWordSearches(){
    $import_data = file_get_contents("http://en.superbook.tv/a/admin/wordsearch_export");
    $json = json_decode($import_data);
    $batch_operations = [];
    foreach($json as $nid6=>$ws_topic){
      $batch_operations[] = array('\Drupal\sb_api_helper\Utilities\Import\Characters::importWordSearchTopic', ["nid6"=>$nid6,"ws_topic"=>$ws_topic]);
    }
    $batch = [
			'title' => "Importing WordSearch Topics",
			'operations' => $batch_operations,
			//'finished' => '\Drupal\sb_content\Controller\ContentController::finishBookSync',
			//'file' => 'path_to_file_containing_myfunctions',
		];
		batch_set($batch);
    return batch_process('/admin/content');
  }
  public static function importWordSearchTopic($nid6, $ws_topic){
    $nodes_saved = 0;
    $ignored_nodes = 0;
    $invalid_nodes = [];
    $iterations = 0;
    $bad_nodes = [];
    $db = \Drupal\Core\Database\Database::getConnection();
    $existing_nodes = [];
    $result = $db->select("node__field_nid6","n6")
      ->fields("n6",["entity_id","field_nid6_value"])
      ->execute();
    foreach($result as $row){
      $existing_nodes[$row->field_nid6_value] = $row->entity_id;
    }
    $node = null;
    if (!isset($ws_topic->translations->en->title)){
      return;
    }
    if (isset($existing_nodes[$nid6])){
      $node_has_changes = false;

      $node = node_load($existing_nodes[$nid6]);
      if (!$node->hasTranslation("en")){
        $node = $node->addTranslation("en");
      }
      $node->set('title', $ws_topic->translations->en->title);
      if ($ws_topic->translations->en->short_title != null){
        $node->set('field_short_title', $ws_topic->translations->en->short_title);
      }
      $node->set('field_ws_words',$ws_topic->translations->en->words);
      $node->set('field_bible_references',[$ws_topic->bible_reference]);
      if (isset($ws_topic->tags)){
        if (count($ws_topic->tags) > 0){
          $topic_array = [];
          foreach($ws_topic->tags as $tag_id){
            if (isset($existing_nodes[$tag_id])){
              $topic_array[] = $existing_nodes[$tag_id];
            }
          }
          $node->set('field_bible_topics',$topic_array);
        }
      }
      $nodes_saved++;
      $node->save();
      //}

      $valid_node = true;
    }
    else{

      $node = Node::create([
        'type'        => 'topical_word_search',
        'title'       => $ws_topic->translations->en->title,
        'field_nid6' => $nid6,
        'field_master_content_key'=>$nid6,
        'langcode' => 'en'
      ]);
      $nodes_saved++;
      $node->save();
      $valid_node = true;

    }
    if($valid_node){
      foreach($ws_topic->translations as $language=>$translation){
        $translation_changed = false;
        if (isset($translation->title) && $translation->title != null && $translation->title != ""){
          if($language != "en"){
            if($language != "tl"){
              if($language != "it"){
                if($language != "et"){
                  if($language != "hy"){
                    if($language != "bn"){
                      if($language != "ta"){
                        if($language != "te"){
                          if($language != "en-ie"){
                            if($language != "en-id"){
                              if($language != "en-ke"){
                                if($language != "zxx"){
                                  if (!$node->hasTranslation($language)){
                                    $translated_node = $node->addTranslation($language);
                                    $translation_changed = true;
                                  }
                                  $translated_node = $node->getTranslation($language);

                                    //error_log("changing title to ".$translation->title. "for $nid6,$language");
                                  $translated_node->set('title', $translation->title);
                                  if ($translation->short_title != null){
                                    $translated_node->set('field_short_title', $translation->short_title);
                                  }
                                  $words = [];
                                  foreach($translation->words as $word){
                                    if (mb_strlen($word) <= 10){
                                      $words[] = $word;
                                    }
                                  }
                                  $translated_node->set('field_ws_words',$words);
                                  //	$translation_changed = true;

                                  $nodes_saved++;
                                  $translated_node->save();
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
