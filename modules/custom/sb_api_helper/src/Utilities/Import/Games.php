<?php
/**
 * @file
 * Contains \Drupal\sb_api_helper\Utilities\Import\Games.
 */

namespace Drupal\sb_api_helper\Utilities\Import;
use Drupal\node\Entity\Node;
use Drupal\sb_api_helper\Utilities\Import\Misc;
class Games{
  public static function importGames($page){
    \set_time_limit(36000);
    ini_set('memory_limit','384M');
    //header('X-Accel-Buffering: no');
    echo ".";
    $existing_nodes = [];
    $result = db_query("select entity_id, field_nid6_value from node__field_nid6");
    $data = $result->fetchAll();
    foreach($data as $datum){
      $existing_nodes[$datum->field_nid6_value] = $datum->entity_id;
    }

    $import_data = file_get_contents("http://en.superbook.tv/a/admin/export_games");
    $games = json_decode($import_data);
    $per_page = 1;
    $offset = $page*$per_page;

    $games_processed = 0;
    $saves = 0;
    foreach($games as $nid6=>$game){

        if (intval($nid6) != 8161426){
          if (isset($existing_nodes[$nid6])){
            $node = node_load($existing_nodes[$nid6]);
            if ( ($games_processed >= $offset) && ($games_processed < ($offset+$per_page))) {
              if (!$node->hasTranslation("en")){
                $node = $node->addTranslation("en");
              }
              $node = $node->getTranslation("en");
              $node->set('title', $game->translations->en->title);
              $node->set('field_short_description', $game->translations->en->short_description);
              $node->set('field_long_description', $game->translations->en->long_description);
              $node->set('field_string_list', $game->translations->en->string_list);
              $filename = "en".$nid6;
              if (isset($game->app_image_path) && $game->app_image_path != ""){
                $node->set('field_game_app_image', Misc::downloadImage("games","field_game_app_image",$nid6,$game->app_image_path));
              }
              if (isset($game->background_image_path) && $game->background_image_path != ""){
                $node->set('field_game_background_image', Misc::downloadImage("games","field_game_background_image",$filename,$game->background_image_path));
              }
              if (isset($game->translations->en->image_38x32_path) && $game->translations->en->image_38x32_path != ""){
                $node->set('field_game_image_38x32', Misc::downloadImage("games","field_game_image_38x32",$filename,$game->translations->en->image_38x32_path));
              }
              if (isset($game->translations->en->image_65x50_path) && $game->translations->en->image_65x50_path != ""){
                $node->set('field_game_image_65x50', Misc::downloadImage("games","field_game_image_65x50",$filename,$game->translations->en->image_65x50_path));
              }
              if (isset($game->translations->en->image_87x60_path) && $game->translations->en->image_87x60_path != ""){
                $node->set('field_game_image_87x60', Misc::downloadImage("games","field_game_image_87x60",$filename,$game->translations->en->image_87x60_path));
              }
              if (isset($game->translations->en->image_115x72_path) && $game->translations->en->image_115x72_path != ""){
                $node->set('field_game_image_115x72', Misc::downloadImage("games","field_game_image_115x72",$filename,$game->translations->en->image_115x72_path));
              }
              if (isset($game->translations->en->image_140x90_path) && $game->translations->en->image_140x90_path != ""){
                $node->set('field_game_image_140x90', Misc::downloadImage("games","field_game_image_140x90",$filename,$game->translations->en->image_140x90_path));
              }
              if (isset($game->translations->en->image_300x209_path) && $game->translations->en->image_300x209_path != ""){
                $node->set('field_game_image_300x209', Misc::downloadImage("games","field_game_image_300x209",$filename,$game->translations->en->image_300x209_path));
              }
              if (isset($game->translations->en->image_1024x490_path) && $game->translations->en->image_1024x490_path != ""){
                $node->set('field_game_image_1024x490', Misc::downloadImage("games","field_game_image_1024x490",$filename,$game->translations->en->image_1024x490_path));
              }
              echo "(saving master node, existing, $games_processed)";
              ob_flush();
              flush();
              $node->save();
              $saves++;
            }
          }
          else{
            $node = Node::create([
              'type'        => 'game',
              'title'       => $game->translations->en->title,
              'field_nid6' => $nid6,
              'field_game_identifier' => $game->game_id,
              'field_long_description' => $game->translations->en->long_description,
              'field_short_description' =>$game->translations->en->short_description,
              'field_string_list' => $game->translations->en->string_list,
              'langcode' => 'en'
            ]);
            $filename = "en".$nid6;
            if (isset($game->app_image_path) && $game->app_image_path != ""){
              $node->set('field_game_app_image', Misc::downloadImage("games","field_game_app_image",$nid6,$game->app_image_path));
            }
            if (isset($game->background_image_path) && $game->background_image_path != ""){
              $node->set('field_game_background_image', Misc::downloadImage("games","field_game_background_image",$filename,$game->background_image_path));
            }
            if (isset($game->translations->en->image_38x32_path) && $game->translations->en->image_38x32_path != ""){
              $node->set('field_game_image_38x32', Misc::downloadImage("games","field_game_image_38x32",$filename,$game->translations->en->image_38x32_path));
            }
            if (isset($game->translations->en->image_65x50_path) && $game->translations->en->image_65x50_path != ""){
              $node->set('field_game_image_65x50', Misc::downloadImage("games","field_game_image_65x50",$filename,$game->translations->en->image_65x50_path));
            }
            if (isset($game->translations->en->image_87x60_path) && $game->translations->en->image_87x60_path != ""){
              $node->set('field_game_image_87x60', Misc::downloadImage("games","field_game_image_87x60",$filename,$game->translations->en->image_87x60_path));
            }
            if (isset($game->translations->en->image_115x72_path) && $game->translations->en->image_115x72_path != ""){
              $node->set('field_game_image_115x72', Misc::downloadImage("games","field_game_image_115x72",$filename,$game->translations->en->image_115x72_path));
            }
            if (isset($game->translations->en->image_140x90_path) && $game->translations->en->image_140x90_path != ""){
              $node->set('field_game_image_140x90', Misc::downloadImage("games","field_game_image_140x90",$filename,$game->translations->en->image_140x90_path));
            }
            if (isset($game->translations->en->image_300x209_path) && $game->translations->en->image_300x209_path != ""){
              $node->set('field_game_image_300x209', Misc::downloadImage("games","field_game_image_300x209",$filename,$game->translations->en->image_300x209_path));
            }
            if (isset($game->translations->en->image_1024x490_path) && $game->translations->en->image_1024x490_path != ""){
              $node->set('field_game_image_1024x490', Misc::downloadImage("games","field_game_image_1024x490",$filename,$game->translations->en->image_1024x490_path));
            }
            echo "(saving master node, new)";
            $node->save();
            $saves++;
          }
          foreach($game->translations as $language=>$translation) {
            if ( ($games_processed >= $offset) && ($games_processed < ($offset+$per_page))) {
              echo ".";
              ob_flush();
              flush();
              if (($language != "en") &&
                 ($language != "tl") &&
                 ($language != "it") &&
                 ($language != "et") &&
                 ($language != "hy") &&
                 ($language != "bn") &&
                 ($language != "ta") &&
                 ($language != "te") &&
                 ($language != "en-ie") &&
                 ($language != "en-id") &&
                 ($language != "en-ke")) {
                  if (!$node->hasTranslation($language)){
                    $translated_node = $node->addTranslation($language);
                    $translated_node->setTitle("tmp");
                    $translated_node->save();
                  }
                  $translated_node = $node->getTranslation($language);
                  $translated_node->set('title', $translation->title);
                  $translated_node->set('field_short_description', $translation->short_description);
                  $translated_node->set('field_long_description', $translation->long_description);
                  $translated_node->set('field_string_list', $translation->string_list);
                  $filename = $language.$nid6;
                  if (isset($translation->image_38x32_path) && $translation->image_38x32_path != ""){
                    $translated_node->set('field_game_image_38x32', Misc::downloadImage("games","field_game_image_38x32",$filename,$translation->image_38x32_path));
                  }
                  if (isset($translation->image_65x50_path) && $translation->image_65x50_path != ""){
                    $translated_node->set('field_game_image_65x50', Misc::downloadImage("games","field_game_image_65x50",$filename,$translation->image_65x50_path));
                  }
                  if (isset($translation->image_87x60_path) && $translation->image_87x60_path != ""){
                    $translated_node->set('field_game_image_87x60', Misc::downloadImage("games","field_game_image_87x60",$filename,$translation->image_87x60_path));
                  }
                  if (isset($translation->image_115x72_path) && $translation->image_115x72_path != ""){
                    $translated_node->set('field_game_image_115x72', Misc::downloadImage("games","field_game_image_115x72",$filename,$translation->image_115x72_path));
                  }
                  if (isset($translation->image_140x90_path) && $translation->image_140x90_path != ""){
                    $translated_node->set('field_game_image_140x90', Misc::downloadImage("games","field_game_image_140x90",$filename,$translation->image_140x90_path));
                  }
                  if (isset($translation->image_300x209_path) && $translation->image_300x209_path != ""){
                    $translated_node->set('field_game_image_300x209', Misc::downloadImage("games","field_game_image_300x209",$filename,$translation->image_300x209_path));
                  }
                  if (isset($translation->image_1024x490_path) && $translation->image_1024x490_path != ""){
                    $translated_node->set('field_game_image_1024x490', Misc::downloadImage("games","field_game_image_1024x490",$filename,$translation->image_1024x490_path));
                  }
                  echo "(saving translation)";
                  $translated_node->save();
                  $saves++;
                }
            }
          $games_processed++;
        }
      }
      $games_processed++;
    }
    if ($games_processed > ($offset+$per_page)) {
      echo "imported $saves game translations, page $page of $games_processed, starting next\n";
      echo "<script>window.location.href='/api_helper/import_games/".($page+1)."';</script>";
    }
    else{
      echo "done";
    }
    exit;

    return array(
      '#type' => 'markup',
      '#markup' => "Finished.",
    );
  }

  public static function importGameStrings(){
    \set_time_limit(36000);
    ini_set('memory_limit','384M');
    $r = scandir("modules/custom/sb_api_helper/data/game_string_json");
    $files = [];
    $unique_strings = [];
    for($i = 2; $i < count($r); $i++){
      $files[$r[$i]] = [
        "path"=>str_replace("/json",".json",str_replace(".","/",$r[$i])),
        "strings"=>[]
      ];
      $filename = "modules/custom/sb_api_helper/data/game_string_json/".$r[$i];
      $contents = file_get_contents($filename);
      $data = json_decode($contents);
      $format_type = "B";
      $case = "upper";
      if (isset($data->data)){
        $data_root = $data->data;
        $format_type = "A";
        if (isset($data->data->format)){
          $format_type = "C";
        }
      }
      if($format_type == "B"){
        $data_root = $data->en->data;
      }
      $files[$r[$i]]["format_type"]=$format_type;
      foreach($data_root as $string_id=>$game_string){
        if ($string_id != "format"){
          if (isset($game_string->TEXT)){
            $files[$r[$i]]["case"]="upper";
            $files[$r[$i]]["strings"][$string_id]["text"] = (string) $game_string->TEXT;
            foreach($game_string->STYLE as $styleName=>$styleValue){
              if($styleName == "wordWrap"){
                if (isset($styleValue->useAdvancedWrap)){
                  $files[$r[$i]]["strings"][$string_id]["style"][$styleName] = "advanced";
                  $files[$r[$i]]["strings"][$string_id]["style"]["wordWrapWidth"] = (string) $styleValue->width;
                }
                else{
                  $files[$r[$i]]["strings"][$string_id]["style"][$styleName] = "true";
                }
              }
              $files[$r[$i]]["strings"][$string_id]["style"][$styleName] = (string) $styleValue;
            }
          }
          else{
            if (isset($game_string->text)){
              $files[$r[$i]]["case"]="lower";
              $files[$r[$i]]["strings"][$string_id]["text"] = (string) $game_string->text;
              foreach($game_string->style as $styleName=>$styleValue){
                if($styleName == "wordWrap"){
                  if (isset($styleValue->useAdvancedWrap)){
                    $files[$r[$i]]["strings"][$string_id]["style"][$styleName] = "advanced";
                    $files[$r[$i]]["strings"][$string_id]["style"]["wordWrapWidth"] = (string) $styleValue->width;
                  }
                  else{
                    $files[$r[$i]]["strings"][$string_id]["style"][$styleName] = "true";
                  }
                }
                else{
                  $files[$r[$i]]["strings"][$string_id]["style"][$styleName] = (string) $styleValue;
                }
              }
            }
            else{
              if ($game_string == "NONE"){
                $files[$r[$i]]["endsWithNone"] = true;
              }
              else{
                echo "could not handle:";
                echo $filename;
                print_r($game_string);
              }

            }
          }
        }
      }


    }
    foreach($files as $file){
      foreach($file["strings"] as $string_id=>&$string){
        //echo "$path:\n";
        //print_r($string);
        $already_added = false;
        foreach($unique_strings as &$unique_string){
          if($string_id == $unique_string["id"]){
            if($unique_string["text"] == $string["text"]){
              foreach($unique_string["style"] as $name=>$val){
                //echo "comparing $val and ".$string["style"][$name]."\n";
                if ($val != $string["style"][$name]){
                  break;
                  //echo "it is not the same style\n";
                }
                else{
                  //echo "it is the same style\n";
                }

              }
              $already_added = true;
              if($already_added){
                $unique_string["locations"][] = $file["path"];
              }
            }

          }
        }
        if(!$already_added){
          $string["locations"][] = $file["path"];
          $string["id"]=$string_id;
          $unique_strings[] = $string;
        }

      }
    }
    //echo count($unique_strings);
    $index = 0;
    $key_to_entity_id = [];
    $db = \Drupal\Core\Database\Database::getConnection();
    $query = $db->select("node__field_master_content_key","mck")
      ->fields("mck",["entity_id","field_master_content_key_value"])
      ->condition("mck.field_master_content_key_value","GS_%","LIKE");
    $results = $query->execute();
    foreach($results as $row){
      $key_to_entity_id[$row->field_master_content_key_value] = $row->entity_id;
    }
    $query = $db->select("node__field_master_content_key","mck")
      ->fields("mck",["entity_id","field_master_content_key_value"])
      ->condition("mck.field_master_content_key_value","GSF_%","LIKE");
    $results = $query->execute();
    foreach($results as $row){
      $key_to_entity_id[$row->field_master_content_key_value] = $row->entity_id;
    }
    foreach($unique_strings as &$unique_string){
      $unique_string["key"] = "GS_".$unique_string["id"]."_".$index;
      if($unique_string["text"] == ""){
        $unique_string["text"] = "EMPTYSTRING";
      }

      if (isset($key_to_entity_id[$unique_string["key"]])){
        //load it?
        $node = Node::load($key_to_entity_id[$unique_string["key"]]);
        $node->set('title', $unique_string["text"]);
        $node->set('field_game_string_id', $unique_string["id"]);
        $node->set('field_master_content_key', $unique_string["key"]);
        if (isset($unique_string["style"])) {
          if (isset($unique_string["style"]["align"])) {
            $node->set('field_align',$unique_string["style"]["align"]);
          }
          if (isset($unique_string["style"]["dropShadow"])) {
            $node->set('field_drop_shadow',$unique_string["style"]["dropShadow"]);
          }
          if (isset($unique_string["style"]["fontFamily"])) {
            $node->set('field_font_family',$unique_string["style"]["fontFamily"]);
          }
          if (isset($unique_string["style"]["fontSize"])) {
            $node->set('field_font_size',$unique_string["style"]["fontSize"]);
          }
          if (isset($unique_string["style"]["fontWeight"])) {
            $node->set('field_font_weight',$unique_string["style"]["fontWeight"]);
          }
          if (isset($unique_string["style"]["fill"])) {
            $node->set('field_text_color',$unique_string["style"]["fill"]);
          }
          if (isset($unique_string["style"]["wordWrap"])) {
            $node->set('field_word_wrap',$unique_string["style"]["wordWrap"]);
          }
          if (isset($unique_string["style"]["wordWrapWidth"])) {
            $node->set('field_word_wrap_width',$unique_string["style"]["wordWrapWidth"]);
          }
        }
        $node->save();
      }
      else{
        $node = Node::create([
          'type'        => 'game_string',
          'title'       => $unique_string["text"],
          'field_game_string_id'=>$unique_string["id"],
          'field_master_content_key' => $unique_string["key"],
          'langcode' => 'en'
        ]);
         if (isset($unique_string["style"])) {
           if (isset($unique_string["style"]["align"])) {
             $node->set('field_align',$unique_string["style"]["align"]);
           }
           if (isset($unique_string["style"]["dropShadow"])) {
             $node->set('field_drop_shadow',$unique_string["style"]["dropShadow"]);
           }
           if (isset($unique_string["style"]["fontFamily"])) {
             $node->set('field_font_family',$unique_string["style"]["fontFamily"]);
           }
           if (isset($unique_string["style"]["fontSize"])) {
             $node->set('field_font_size',$unique_string["style"]["fontSize"]);
           }
           if (isset($unique_string["style"]["fontWeight"])) {
             $node->set('field_font_weight',$unique_string["style"]["fontWeight"]);
           }
           if (isset($unique_string["style"]["fill"])) {
             $node->set('field_text_color',$unique_string["style"]["fill"]);
           }
           if (isset($unique_string["style"]["wordWrap"])) {
             $node->set('field_word_wrap',$unique_string["style"]["wordWrap"]);
           }
           if (isset($unique_string["style"]["wordWrapWidth"])) {
             $node->set('field_word_wrap_width',$unique_string["style"]["wordWrapWidth"]);
           }
         }

         $node->save();
         $new_entity_id = $node->id();
         $key_to_entity_id[$unique_string["key"]] = $new_entity_id;
      }
      $index++;
    }
    foreach($files as $key=>$file){

      $mck = "GSF_".str_replace(".json","",$key);
      if (isset($key_to_entity_id[$mck])){
        //load it
        $node = Node::load($key_to_entity_id[$mck]);
        $node->set('title',$key);
      }
      else{
        $node = Node::create([
          'type'        => 'game_localization_set',
          'title'       => $key,
          'langcode' => 'en'
        ]);


      }
      $node->set('field_master_content_key',$mck);
      if (isset($file["path"])){
        $node->set("field_path",$file["path"]);
      }
      if (isset($file["format_type"])){
        $node->set("field_data_structure_type",$file["format_type"]);
      }


      //use $key_to_entity_id to find corresponding strings.
      $stringValues = [];

      foreach($unique_strings as $object){
        foreach($object["locations"] as $location){
          if($location == $file["path"]){
            $stringValues[] = ["target_id"=>$key_to_entity_id[$object["key"]]];
          }
        }
      }
      //print_r($stringValues);
      $node->set('field_localization_string_list',$stringValues);
      $node->save();
    }
    return array(
      '#type' => 'markup',
      '#markup' => "Finished it.",
    );
  }
}
