<?php
/**
 * @file
 * Contains \Drupal\sb_api_helper\Utilities\Import\TimePeriods.
 */

namespace Drupal\sb_api_helper\Utilities\Import;
use Drupal\node\Entity\Node;
use Drupal\sb_api_helper\Utilities\Import\Misc;
class Wallpapers{
  public static function importWallpapers(){
    $import_data = file_get_contents("http://en.superbook.tv/a/admin/export_wallpapers");
    $wallpapers = json_decode($import_data);

    $batch_operations = [];
    foreach($wallpapers as $nid6=>$wallpaper){
      $batch_operations[] = array('\Drupal\sb_api_helper\Utilities\Import\Wallpapers::batchImportWallpaper', ["nid6"=>$nid6,"wallpaper"=>$wallpaper]);
    }
    $batch = [
			'title' => "Importing Wallpapers",
			'operations' => $batch_operations,
			//'finished' => '\Drupal\sb_content\Controller\ContentController::finishBookSync',
			//'file' => 'path_to_file_containing_myfunctions',
		];
		batch_set($batch);
  		// Only needed if not inside a form _submit handler.
  		// Setting redirect in batch_process.
		return batch_process('/admin/content');
  }
  public static function batchImportWallpaper($nid6, $wallpaper){
    $db = \Drupal\Core\Database\Database::getConnection();
    $existing_nodes = [];
    $result = $db->select("node__field_nid6","n6")
      ->fields("n6",["entity_id","field_nid6_value"])
      ->execute();
    foreach($result as $row){
      $existing_nodes[$row->field_nid6_value] = $row->entity_id;
    }
    if(!isset($wallpaper->title)){
      return;
    }
    $node = null;
    if (isset($existing_nodes[$nid6])){
      $node = Node::load($existing_nodes[$nid6]);
    }
    else{

      $node = Node::create([
        "type"=>"wallpaper",
        "title"=>$wallpaper->title,
        "field_nid6"=>$nid6,
        "field_master_content_key"=>$nid6
      ]);
    }
    $node->set("title",(string)$wallpaper->title);
    $time_periods = [];
    foreach($wallpaper->time_periods as $tp_nid6){
      if (isset($existing_nodes[$tp_nid6])){
        $time_periods[] = $existing_nodes[$tp_nid6];
      }
      else{
        die("could not find [$tp_nid6] time period node");
      }

    }
    if (count($time_periods) > 0){
      $node->set('field_bible_time_period',$time_periods);
    }
    $topics = [];
    foreach($wallpaper->tags as $tag_nid6){
      if (isset($existing_nodes[$tag_nid6])){
        $topics[] = $existing_nodes[$tag_nid6];
      }
      else{
        die("could not find [$tag_nid6] bible topic node");
      }

    }
    if (count($topics) > 0){
      $node->set('field_bible_topics',$topics);
    }
    $book_references = [];
    foreach($wallpaper->book_refs as $book_ref_nid6){
      $book_key = Misc::bibleBookFromNid6($book_ref_nid6);
      if ($book_key === false){
        die("unhandled book reference nid: $book_ref_nid6");
      }
      $book_references[] = $book_key;
    }
    if (count($book_references) > 0){
      $node->set('field_bible_book_reference',$book_references);
    }

    if (intval($wallpaper->status) == 1){
      $node->setPublished(true);
    }
    else{
      $node->setPublished(false);
    }
    $source_field = "filepath";
    $dst = "wallpapers/source/";
    $node_field = "field_wallpaper_image";
    if ($wallpaper->$source_field != null){
      $source = $wallpaper->$source_field;
      $image_file_data = file_get_contents('https://us-en.superbook.cbn.com/'.$source);
      $ext = ".png";
      $last_dot = mb_strrpos($source,".");
      if ($last_dot > 0 ){
        $ext = mb_substr($source,$last_dot, strlen($source));
      }
      $last_slash = mb_strrpos($source,"/");
      $filename = $nid6."_".mb_substr($source,$last_slash, strlen($source));
      $filename = str_replace($ext,"",$filename);
      $filename = Misc::cleanFileName($filename);
      $filename .= $ext;
      $new_path = 'public://'.$dst. $filename;
      $new_image_file = file_save_data($image_file_data, $new_path, FILE_EXISTS_REPLACE);
      $node->set($node_field,["target_id"=>$new_image_file->id()]);
    }
    $node->save();
  }
}
