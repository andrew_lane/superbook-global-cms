<?php
/**
 * @file
 * Contains \Drupal\sb_api_helper\Utilities\Import\Trivia.
 */

namespace Drupal\sb_api_helper\Utilities\Import;
use Drupal\node\Entity\Node;
class Trivia{
  public static function importTrivia(){
    \set_time_limit(36000);
    ini_set('memory_limit','384M');

    $import_data = file_get_contents("http://en.superbook.tv/a/admin/trivia_export");

    $json = json_decode($import_data);
    $batch_operations = [];
    foreach($json as $nid6=>$trivia_question){
      $batch_operations[] = array('\Drupal\sb_api_helper\Utilities\Import\Trivia::batchImportTriviaQuestion', ["nid6"=>$nid6,"trivia_question"=>$trivia_question]);
    }

    $batch = [
			'title' => "Importing Trivia Questions",
			'operations' => $batch_operations,
			//'finished' => '\Drupal\sb_content\Controller\ContentController::finishBookSync',
			//'file' => 'path_to_file_containing_myfunctions',
		];
		batch_set($batch);
  		// Only needed if not inside a form _submit handler.
  		// Setting redirect in batch_process.
		return batch_process('/admin/content');
  }
  public static function batchImportTriviaQuestion($nid6, $trivia_question){
    $nodes_saved = 0;
    $ignored_nodes = 0;
    $invalid_nodes = [];
    $db = \Drupal\Core\Database\Database::getConnection();
    $existing_nodes = [];
    $result = $db->select("node__field_nid6","n6")
      ->fields("n6",["entity_id","field_nid6_value"])
      ->execute();
    foreach($result as $row){
      $existing_nodes[$row->field_nid6_value] = $row->entity_id;
    }
    $node = null;
    if (!isset($trivia_question->translations->en->title)){
      return;
    }
    if (isset($existing_nodes[$nid6])){
      $node_has_changes = false;

      $node = node_load($existing_nodes[$nid6]);
      if (!$node->hasTranslation("en")){
        $node = $node->addTranslation("en");
      }
      $node->set('title', $trivia_question->translations->en->title);
      if ($trivia_question->translations->en->short_title != null){
        $node->set('field_short_title', $trivia_question->translations->en->short_title);
      }
      $node->set('field_trivia_correct_answers',$trivia_question->translations->en->correct_answers);
      $node->set('field_trivia_incorrect_answers',$trivia_question->translations->en->incorrect_answers);
      $node->set('field_bible_references',$trivia_question->bible_reference);
      if (isset($trivia_question->tags)){
        if (count($trivia_question->tags) > 0){
          $topic_array = [];
          foreach($trivia_question->tags as $tag_id){
            if (isset($existing_nodes[$tag_id])){
              $topic_array[] = $existing_nodes[$tag_id];
            }
          }
          $node->set('field_bible_topics',$topic_array);
        }
      }
      $nodes_saved++;
      $node->save();
      $last_nid = $node->id();
      //}

      $valid_node = true;
    }
    else{
      $node = Node::create([
        'type'        => 'trivia',
        'title'       => $trivia_question->translations->en->title,
        'field_trivia_correct_answers'=>$trivia_question->translations->en->correct_answers,
        'field_trivia_incorrect_answers'=>$trivia_question->translations->en->incorrect_answers,
        'field_nid6' => $nid6,
        'field_master_content_key'=>$nid6,
        'langcode' => 'en'
      ]);
      $nodes_saved++;
      $node->save();
      $valid_node = true;

    }
    if($valid_node){
      foreach($trivia_question->translations as $language=>$translation){
        $translation_changed = false;
        if (isset($translation->title) && $translation->title != null && $translation->title != ""){
          if($language != "en"){
            if($language != "tl"){
              if($language != "it"){
                if($language != "et"){
                  if($language != "hy"){
                    if($language != "bn"){
                      if($language != "ta"){
                        if($language != "te"){
                          if($language != "en-ie"){
                            if($language != "en-id"){
                              if($language != "en-ke"){
                                if($language != "ms"){
                                  if($language != "zxx"){
                                    if($language != ""){
                                      if (!$node->hasTranslation($language)){
                                        $translated_node = $node->addTranslation($language);
                                        $translation_changed = true;
                                      }
                                      $translated_node = $node->getTranslation($language);
                                      $translated_node->set('title', $translation->title);
                                      if ($translation->short_title != null){
                                        $translated_node->set('field_short_title', $translation->short_title);
                                      }
                                      $translated_node->set('field_trivia_correct_answers',$translation->correct_answers);
                                      $translated_node->set('field_trivia_incorrect_answers',$translation->incorrect_answers);
                                      $nodes_saved++;
                                      $translated_node->save();
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  public static function fixTriviaQuestions(){
    $db = \Drupal\Core\Database\Database::getConnection();
    $query1 = "select distinct entity_id from node__field_trivia_correct_answers where field_trivia_correct_answers_value like '%all of the above%'";
    $query2 = "select distinct entity_id from node__field_trivia_correct_answers where field_trivia_correct_answers_value like '%none of the above%'";
    $query3 = "select distinct entity_id from node__field_trivia_correct_answers where field_trivia_correct_answers_value like 'Both%and%' and entity_id not in (917554)";
    $query4 = "select distinct entity_id from node__field_trivia_incorrect_answers where field_trivia_incorrect_answers_value like '%all of the above%'";
    $query5 = "select distinct entity_id from node__field_trivia_incorrect_answers where field_trivia_incorrect_answers_value like '%none of the above%'";
    $query6 = "select distinct entity_id from node__field_trivia_incorrect_answers where field_trivia_incorrect_answers_value like 'Both%and%' and entity_id NOT IN (917554,916873)";
    $count = 0;
    $results = $db->query($query1);
    foreach($results as $row){
      $count++;
      $node = Node::load($row->entity_id);
      $languages = $node->getTranslationLanguages(false);
      foreach($languages as $langcode=>$lang_object){
        echo "trying to correct node: $row->entity_id ($langcode)\n";
        $t_node = $node->getTranslation($langcode);
        $prev_correct_answers = $t_node->get('field_trivia_correct_answers')->getValue();
        $prev_incorrect_answers = $t_node->get('field_trivia_incorrect_answers')->getValue();
        if (count($prev_correct_answers) == 1){
          if (count($prev_incorrect_answers) > 1){
            $t_node->set("field_trivia_correct_answers",$prev_incorrect_answers);
            $t_node->set("field_trivia_incorrect_answers",[]);
            $t_node->save();
            echo "saved node $row->entity_id ($langcode)\n";
          }
        }
      }
      $count++;
      $prev_correct_answers = $node->get('field_trivia_correct_answers')->getValue();
      $prev_incorrect_answers = $node->get('field_trivia_incorrect_answers')->getValue();
      if (count($prev_correct_answers) == 1){
        if (count($prev_incorrect_answers) > 1){
          $node->set("field_trivia_correct_answers",$prev_incorrect_answers);
          $node->set("field_trivia_incorrect_answers",[]);
          $node->save();
          echo "saved node $row->entity_id (master)\n";
        }
      }
    }
    echo "record count 1: $count\n";
    $count = 0;
    $results = $db->query($query2);
    foreach($results as $row){
      $count++;
      $node = Node::load($row->entity_id);
      $languages = $node->getTranslationLanguages(false);
      foreach($languages as $langcode=>$lang_object){
        $count++;
        echo "trying to correct node: $row->entity_id ($langcode)\n";
        $t_node = $node->getTranslation($langcode);
        $prev_correct_answers = $t_node->get('field_trivia_correct_answers')->getValue();
        $prev_incorrect_answers = $t_node->get('field_trivia_incorrect_answers')->getValue();
        if (count($prev_correct_answers) == 1){
          $t_node->set("field_trivia_correct_answers",[]);
          $t_node->save();
          echo "saved node $row->entity_id ($langcode)\n";
        }
      }
      $prev_correct_answers = $node->get('field_trivia_correct_answers')->getValue();
      $prev_incorrect_answers = $node->get('field_trivia_incorrect_answers')->getValue();
      if (count($prev_correct_answers) == 1){
        $node->set("field_trivia_correct_answers",[]);
        $node->save();
        echo "saved node $row->entity_id (master)\n";
      }
    }
    echo "record count 2: $count\n";
    $count = 0;
    $results = $db->query($query3);
    foreach($results as $row){
      $count++;
      $node = Node::load($row->entity_id);
      $languages = $node->getTranslationLanguages(false);
      $master_correct_answers = $node->get('field_trivia_correct_answers')->getValue();

      $type = "Unknown";
      switch($master_correct_answers[0]["value"]){
        case "Both A and B.":
        case "Both A and B are correct.":
          $type = "AB";
          break;
        case "Both A and C.":
        case "Both A. and C. are correct.":
        case "Both A and C are correct.":
          $type = "AC";
          break;
        case "Both B and C are correct.":
        case "Both answers B and C are correct.":
          $type = "BC";
          break;
      }
      if($type == "Unknown"){
        echo $row->entity_id .": ".$master_correct_answers[0]["value"]."\n";
      }
      foreach($languages as $langcode=>$lang_object){
        $count++;
        echo "trying to correct node: $row->entity_id ($langcode)\n";
        $t_node = $node->getTranslation($langcode);
        $prev_correct_answers = $t_node->get('field_trivia_correct_answers')->getValue();
        $prev_incorrect_answers = $t_node->get('field_trivia_incorrect_answers')->getValue();
        if (count($prev_correct_answers) == 1){
          switch($type){
            case "AB":
              $new_correct_answers = [
                0=>$prev_incorrect_answers[0],
                1=>$prev_incorrect_answers[1]
              ];
              $t_node->set("field_trivia_correct_answers",$new_correct_answers);
              $new_incorrect_answers = [
                0=>$prev_incorrect_answers[2]
              ];
              $t_node->set("field_trivia_incorrect_answers",$new_incorrect_answers);
              break;
            case "AC":
              $new_correct_answers = [
                0=>$prev_incorrect_answers[0],
                1=>$prev_incorrect_answers[2]
              ];
              $t_node->set("field_trivia_correct_answers",$new_correct_answers);
              $new_incorrect_answers = [
                0=>$prev_incorrect_answers[1]
              ];
              $t_node->set("field_trivia_incorrect_answers",$new_incorrect_answers);
              break;
            case "BC":
              $new_correct_answers = [
                0=>$prev_incorrect_answers[1],
                1=>$prev_incorrect_answers[2]
              ];
              $t_node->set("field_trivia_correct_answers",$new_correct_answers);
              $new_incorrect_answers = [
                0=>$prev_incorrect_answers[0]
              ];
              $t_node->set("field_trivia_incorrect_answers",$new_incorrect_answers);
              break;
          }
          $t_node->save();
          echo "saved node $row->entity_id ($langcode)\n";
        }
        $prev_correct_answers = $node->get('field_trivia_correct_answers')->getValue();
        $prev_incorrect_answers = $node->get('field_trivia_incorrect_answers')->getValue();
        if (count($prev_correct_answers) == 1){
          switch($type){
            case "AB":
              $new_correct_answers = [
                0=>$prev_incorrect_answers[0],
                1=>$prev_incorrect_answers[1]
              ];
              $node->set("field_trivia_correct_answers",$new_correct_answers);
              $new_incorrect_answers = [
                0=>$prev_incorrect_answers[2]
              ];
              $node->set("field_trivia_incorrect_answers",$new_incorrect_answers);
              break;
            case "AC":
              $new_correct_answers = [
                0=>$prev_incorrect_answers[0],
                1=>$prev_incorrect_answers[2]
              ];
              $node->set("field_trivia_correct_answers",$new_correct_answers);
              $new_incorrect_answers = [
                0=>$prev_incorrect_answers[1]
              ];
              $node->set("field_trivia_incorrect_answers",$new_incorrect_answers);
              break;
            case "BC":
              $new_correct_answers = [
                0=>$prev_incorrect_answers[1],
                1=>$prev_incorrect_answers[2]
              ];
              $node->set("field_trivia_correct_answers",$new_correct_answers);
              $new_incorrect_answers = [
                0=>$prev_incorrect_answers[0]
              ];
              $node->set("field_trivia_incorrect_answers",$new_incorrect_answers);
              break;
          }
          $node->save();
          echo "saved node $row->entity_id (master)\n";
        }
      }
    }
    echo "record count 3: $count\n";
    $count = 0;
    $results = $db->query($query5);
    foreach($results as $row){
      $count++;
      $node = Node::load($row->entity_id);
      //$master_incorrect_answers = $node->get('field_trivia_incorrect_answers')->getValue();
      echo "$row->entity_id needs manual editing (remove \"none of the above\" from incorrect answers):  \n";
    }
    echo "record count 5: $count\n";

    $count = 0;
    $results = $db->query($query6);
    foreach($results as $row){
      $count++;
      $node = Node::load($row->entity_id);
      //$master_incorrect_answers = $node->get('field_trivia_incorrect_answers')->getValue();
      echo "$row->entity_id needs manual editing (remove \"both x & y\" from incorrect answers):  \n";
    }
    echo "record count 6: $count\n";
    exit;
  }
}
