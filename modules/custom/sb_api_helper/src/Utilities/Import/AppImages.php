<?php
/**
 * @file
 * Contains \Drupal\sb_api_helper\Utilities\Import\AppImages.
 */

namespace Drupal\sb_api_helper\Utilities\Import;
use Drupal\sb_api_helper\Utilities\Import\Misc;
use Drupal\node\Entity\Node;
class AppImages{
  public static function importAppImages($page){
    \set_time_limit(36000);
    ini_set('memory_limit','384M');
    $start_time = \microtime(true);
    $existing_nodes = [];
    $per_page = 20;
    $offset = $per_page*$page;
    $query = "select entity_id, field_nid6_value from node__field_nid6";
    $result = db_query($query);
    $data = $result->fetchAll();
    $nodes_saved = 0;
    foreach($data as $datum){
      $existing_nodes[$datum->field_nid6_value] = $datum->entity_id;
    }
    $import_data = file_get_contents("http://en.superbook.tv/a/admin/export_app_images");
    $data = json_decode($import_data);
    $available_count = count($data);
    $item_number = 0;
    foreach($data as $datum){
      if( ($item_number >= $offset) && ($item_number <= $offset+$per_page)){
        $node = Node::load($existing_nodes[$datum->nid6]);
        if ($node != null){
          $images_to_download = [];
          if (isset($datum->img_516x360)){
            $images_to_download[] = [
              "source"=>$datum->img_516x360,
              "destination_path"=>"images/video/browse/",
              "field"=>"field_browse_image"
            ];
          }
          elseif (isset($datum->img_516x288)){
            $images_to_download[] = [
              "source"=>$datum->img_516x288,
              "destination_path"=>"images/video/browse/",
              "field"=>"field_browse_image"
            ];
          }


          if (isset($datum->img_271x485)){
            $images_to_download[] = [
              "source"=>$datum->img_271x485,
              "destination_path"=>"images/characters/browse/",
              "field"=>"field_browse_image"
            ];
          }
          elseif (isset($datum->img_163x290)){
            $images_to_download[] = [
              "source"=>$datum->img_163x290,
              "destination_path"=>"images/characters/browse/",
              "field"=>"field_browse_image"
            ];
          }

          if (isset($datum->img_743x388)){
            $images_to_download[] = [
              "source"=>$datum->img_743x388,
              "destination_path"=>"images/characters/header/",
              "field"=>"field_profile_header_image"
            ];
          }
          foreach($images_to_download as $image_data){
            $image_file_data = file_get_contents('http://cdn.superbook.cbn.com/'.$image_data["source"]);
            $ext = ".png";
            $last_dot = mb_strrpos($image_data["source"],".");
            if ($last_dot > 0 ){
              $ext = mb_substr($image_data["source"],$last_dot, strlen($image_data["source"]));
            }
            $new_path = 'public://'.$image_data["destination_path"].Misc::cleanFileName($datum->nid6) . $ext;
            $new_image_file = file_save_data($image_file_data, $new_path, FILE_EXISTS_REPLACE);
            $node->set($image_data["field"],["target_id"=>$new_image_file->id()]);
            $node->save();
            $nodes_saved++;
          }
        }
      }

      $item_number++;
    }
    if($nodes_saved != 0){
      echo "imported page $page, starting next\n";
      echo "<script>window.location.href='/api_helper/import_app_images/".($page+1)."';</script>";
    }
    else{
      echo "done";
    }

    return array(
      '#type' => 'markup',
      '#cache'=>['max-age'=>0],
      '#markup' => "Finished $nodes_saved nodes in $end_time seconds. Index: $index. Processed $available_count of $original_count nodes.",
    );
  }
}
